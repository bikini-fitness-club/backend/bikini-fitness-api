"use strict";

const service = require("../service/ClassesSchedulerService");
const {success, badRequest, created} = require("../config/HttpResponse");
const toString = require("../config/ToString");
const validator = require("../config/Validator");

/**
 * Exposed service to create the Class.
 * If the Class configuration is recurring, then it is done
 * the creation of classes on their different days and weeks.
 *
 * @param req
 * @param res
 * @returns {Promise<{body: string, statusCode: *}>}
 */
module.exports.create = async (req, res) => {
    let dto = toString(req.body);
    try {
        let result = await service.create(dto);
        if (result.length > 0) {
            let data = {
                message: "There is already a class scheduled for this time", result
            };
            return badRequest(res, data);
        }

        return created(res, "Classes Schedule created");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
module.exports.list = async (req, res) => {

    let classId = validator.valueOrUndefined(req.query.classId);
    let trainerId = validator.valueOrUndefined(req.query.trainerId);
    let locationId = validator.valueOrUndefined(req.query.locationId);
    let classType = validator.valueOrUndefined(req.query.classType);
    let date = validator.valueOrUndefined(req.query.date);

    let result = await service.listClassDetail(classId, trainerId, locationId, classType, date);
    return success(res, result);
}

/**
 * Exposed service to delete a class scheduled by id.
 *
 * @param req
 * @param res
 * @returns {Promise<{body: string, statusCode: *}>}
 */
module.exports.delete = async (req, res) => {
    let id = req.params.id;
    try {
        await service.delete(id);
        return success(res, "Classes Schedule deleted");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

/**
 * Exposed service to detail class Schedule a participants
 *
 * @param req
 * @param res
 * @returns {Promise<{body: string, statusCode: *}>}
 */
module.exports.getClassesScheduler = async (req, res) => {
    let id = req.params.id;
    try {
        if (validator.undefinedAndEmpty(id)) badRequest(res, "Invalid Parameters");
        let result = await service.getClassesScheduler(id);
        return success(res, result);
    } catch (exception) {
        return badRequest(res, exception);
    }
}

/**
 * Exposed service to update a scheduled class.
 *
 * @param req
 * @param res
 * @returns {Promise<{body: string, statusCode: *}>}
 */
module.exports.update = async (req, res) => {
    let dto = toString(req.body);
    try {
        await service.update(dto);
        return success(res, "Classes Schedule updated");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

/**
 * Exposed service to list the classes allowed to the user
 *
 * @param req
 * @param res
 * @returns {Promise<{body: string, statusCode: *}>}
 */
module.exports.userClassesScheduler = async (req, res) => {
    let userId = req.query === null ? undefined : req.query.userId;
    let campusId = req.query === null ? undefined : req.query.campusId;
    if (validator.undefined(userId) || validator.undefined(campusId)) return badRequest(res, "Invalid Parameters");

    try {
        let result = await service.classSchedulerPerUser(userId, campusId);
        return success(res, result);
    } catch (exception) {
        return badRequest(res, exception);
    }
}

/**
 * Exposed service to register user into schedule class
 *
 * @param req
 * @param res
 * @returns {Promise<{body: string, statusCode: *}>}
 */
module.exports.registerUserClassesSchedulerReserve = async (req, res) => {
    let dto = req.body;
    if (validator.undefinedAndEmpty(dto.userId) || validator.undefinedAndEmpty(dto.classSchedulerId))
        return badRequest(res, "Invalid Parameters");

    try {
        let result = await service.registerUserClassesScheduler(dto);
        return success(res, result);
    } catch (exception) {
        return badRequest(res, exception);
    }
}

/**
 * Method for list reserved classes per UserId
 *
 * @param req
 * @param res
 * @returns {Promise<{body: string, statusCode: *}>}
 */
module.exports.listUserClassesSchedulerReserved = async (req, res) => {
    let userId = req.params.id;
    let campusId = req.query.campusId;
    if (validator.undefinedAndEmpty(userId) || validator.undefined(campusId))
        return badRequest(res, "Invalid Parameters");

    try {
        let result = await service.listUserClassesSchedulerReserved(userId, campusId);
        return success(res, result);
    } catch (exception) {
        return badRequest(res, exception);
    }
}

/**
 *  Method for delete reserved classes
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
module.exports.validateDeleteUserClassesSchedulerReserve = async (req, res) => {
    const userId = req.params.userId;
    const classScheduleReserveId = req.params.classId;
    if (validator.undefinedAndEmpty(userId) || validator.undefinedAndEmpty(classScheduleReserveId))
        return badRequest(res, "Invalid Parameters");

    try {
        let result = await service.validateDeleteUserClassesSchedulerReserve(userId, classScheduleReserveId);
        return success(res, result);
    } catch (exception) {
        return badRequest(res, exception);
    }
}

/**
 *  Method for delete reserved classes
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
module.exports.deleteUserClassesSchedulerReserve = async (req, res) => {
    const userId = req.params.userId;
    const classScheduleReserveId = req.params.classId;
    if (validator.undefinedAndEmpty(userId) || validator.undefinedAndEmpty(classScheduleReserveId))
        return badRequest(res, "Invalid Parameters");

    try {
        await service.deleteUserClassesSchedulerReserve(userId, classScheduleReserveId);
        return success(res, "Class deleted");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

/**
 * Exposed service to detail class Schedule a participants
 *
 * @param req
 * @param res
 * @returns {Promise<{body: string, statusCode: *}>}
 */
module.exports.getDetailClassesScheduler = async (req, res) => {
    let id = req.params.id;
    try {
        if (validator.undefinedAndEmpty(id)) return badRequest(res, "Invalid Parameters");
        let result = await service.getClassesScheduler(id);
        return success(res, result);
    } catch (exception) {
        return badRequest(res, exception);
    }
}

/**
 * List of scheduled classes available for Free Class.
 * The list of classes is by location.
 *
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
module.exports.getClassesSchedulerFreeClass = async (req, res) => {
    let campusId = req.query.campusId;
    if (validator.undefinedAndEmpty(campusId)) return badRequest(res, "Invalid Parameters");
    try {
        let result = await service.getClassesSchedulerFreeClass(campusId);
        return success(res, result);
    } catch (exception) {
        return badRequest(res, exception);
    }
}