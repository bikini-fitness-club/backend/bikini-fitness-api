const authService = require("../service/AuthService");
const validator = require("../config/Validator");
const {success, badRequest} = require("../config/HttpResponse");

module.exports.login = async (req, res) => {
    let credentials = req.body
    let result;
    try {
        result = await authService.validate(credentials.email, credentials.password);
    } catch (exception) {
        return badRequest(res, exception);
    }
    return success(res, result);
}

module.exports.resetPassword = async (req, res) => {
    let dto = req.body;
    try {
        let result = await authService.resetPassword(dto);
        return success(res, result);
    } catch (exception) {
        return badRequest(res, exception);
    }
}

module.exports.validateEmail = async (req, res) => {
    let dto = req.body;
    try {
        if (validator.undefinedAndEmpty(dto.email) ||
            validator.isNotEmail(dto.email)) return badRequest(res, "Parameters Invalid");

        await authService.validateEmail(dto);
        return success(res, "Email valid");
    } catch (exception) {
        return badRequest(res, exception);
    }
}
