"use strict";

const service = require("../service/DashboardService");
const {badRequest, success} = require("../config/HttpResponse");

/**
 *
 * @param req
 * @param res
 * @returns {Promise<{body: *|string, statusCode: *}>}
 */
module.exports.generate = async (req, res) => {
    if (req.query === undefined) badRequest(res, 'Invalid parameters');
    try {
        let result = await service.generate(req.query.userId);
        return success(res, result);
    } catch (exception) {
        return badRequest(res, exception);
    }
}