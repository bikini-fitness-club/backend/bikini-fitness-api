"use strict";

const profileService = require("../service/ProfileService");
const {success, created, badRequest} = require("../config/HttpResponse");
const validator = require("../config/Validator");

const INVALID_PARAMETERS = "Invalid parameter";

module.exports.create = async (req, res) => {
    let dto = req.body;
    try {
        if (validator.undefinedAndEmpty(dto.name)) return badRequest(res, INVALID_PARAMETERS);
        await profileService.create(dto);

        return created(res, "Profile created");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

module.exports.list = async (req, res) => {
    let result = await profileService.list();
    return success(res, result);
}

module.exports.delete = async (req, res) => {
    let id = req.params.id;
    try {
        if (validator.undefinedAndEmpty(id)) return badRequest(res, INVALID_PARAMETERS);
        await profileService.delete(id);

        return success(res, "Profile deleted");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

module.exports.update = async (req, res) => {
    let dto = req.body;
    try {
        if (validator.undefinedAndEmpty(dto.name)) return badRequest(res, INVALID_PARAMETERS);
        await profileService.update(dto);

        return success(res, "Profile updated");
    } catch (exception) {
        return badRequest(res, exception);
    }
}