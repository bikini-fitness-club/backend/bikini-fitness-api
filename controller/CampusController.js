"use strict";

const service = require("../service/CampusService");
const validator = require("../config/Validator");
const {success, created, badRequest} = require("../config/HttpResponse");


module.exports.list = async (req, res) => {
    let result = await service.list();
    return success(res, result);
}

module.exports.create = async (req, res) => {
    await service.create(req.body);
    return created(res, "Campus created");
}

module.exports.delete = async (req, res) => {
    let campusId = req.params.id;
    try {
        await service.delete(campusId);
        return success(res, "Campus deleted")
    } catch (exception) {
        return badRequest(exception);
    }
}

module.exports.update = async (req, res) => {
    let params = req.body;
    try {
        await service.update(params);
        return success(res, "");
    } catch (exception) {
        return badRequest(exception);
    }
}

module.exports.getByUserId = async (req, res) => {
    let userId = req.params.id;
    try {
        if (validator.undefined(userId)) {
            return badRequest(res, "Invalid Parameters");
        }
        let result = await service.getByUserId(userId);
        return success(res, result);
    } catch (exception) {
        return badRequest(exception);
    }

}