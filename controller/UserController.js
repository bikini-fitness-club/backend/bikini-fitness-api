"use strict";
const { success, created, badRequest } = require("../config/HttpResponse");
const userService = require("../service/UserService");
const validator = require("../config/Validator");

const INVALID_PARAMETERS = "Invalid parameter";

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
module.exports.register = async (req, res) => {
  console.log("Register body -> " + JSON.stringify(req.body));

  let params = req.body;
  if (
    params.customer === undefined ||
    params.customer === null ||
    params.line_items === undefined ||
    params.line_items === null
  ) {
    return badRequest(res, INVALID_PARAMETERS);
  }

  let user = params.customer;
  let campusName =
    params.line_items[0].sku || params.line_items[0].variant_title;
  let membershipRelationalId =
    params.line_items[0].variant_id || params.line_items[0].product_id;

  try {
    await userService.register(user, campusName, membershipRelationalId);
    return created(res, "User created");
  } catch (exception) {
    return badRequest(res, exception);
  }
};

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
module.exports.create = async (req, res) => {
  let dto = req.body;
  try {
    await userService.create(dto);
    return created(res, "User created");
  } catch (exception) {
    return badRequest(res, exception);
  }
};

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
module.exports.delete = async (req, res) => {
  let userId = req.params.id;
  try {
    await userService.delete(userId);
    return success(res, "User deleted");
  } catch (exception) {
    return badRequest(res, exception);
  }
};

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
module.exports.list = async (req, res) => {
  if (req.query === undefined) throw "ProfileId not found";
  let profileId = req.query.profileId;
  let result = await userService.list(profileId);

  return success(res, result);
};

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
module.exports.update = async (req, res) => {
  let dto = req.body;
  try {
    await userService.update(dto);
    return success(res, "User updated");
  } catch (exception) {
    return badRequest(res, exception);
  }
};

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
module.exports.createUserFreeClass = async (req, res) => {
  let dto = req.body;
  try {
    if (
      validator.undefined(dto.user) ||
      validator.undefined(dto.class) ||
      validator.undefinedAndEmpty(dto.user.email) ||
      validator.isNotEmail(dto.user.email) ||
      validator.undefinedAndEmpty(dto.user.firstName) ||
      validator.undefinedAndEmpty(dto.user.lastName) ||
      validator.undefinedAndEmpty(dto.user.phone) ||
      validator.undefinedAndEmpty(dto.class.classSchedulerId) ||
      validator.undefined(dto.class.classId)
    )
      return badRequest(res, INVALID_PARAMETERS);

    await userService.createUserFreeClass(dto);
    return success(res, "User Free Class created");
  } catch (exception) {
    return badRequest(res, exception);
  }
};
