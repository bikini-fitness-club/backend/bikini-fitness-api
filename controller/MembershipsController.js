"use strict";

const service = require("../service/MembershipsService");
const {badRequest, created, success} = require("../config/HttpResponse");

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
module.exports.create = async (req, res) => {
    let dto = req.body;
    try {
        await service.create(dto);
        return created(res, "Membership created");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
module.exports.list = async (req, res) => {
    let result = await service.list();
    return success(res, result);
}

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
module.exports.delete = async (req, res) => {
    let id = req.params.id;
    try {
        await service.delete(id);
        return success(res, "Membership deleted");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
module.exports.update = async (req, res) => {
    let dto = req.body;
    try {
        await service.update(dto);
        return success(res, "Membership updated");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
module.exports.associateMembership = async (req, res) => {
    let dto = req.body;
    try {
        await service.associateUser(dto);
        return success(res, "Membership associated");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

/**
 *
 * @param req
 * @param res
 * @returns {Promise<{body: *|string, statusCode: *}>}
 */
module.exports.registerPayment = async (req, res) => {
    let dto = req.body;
    try {
        if (dto.userMembershipId === undefined || dto.paymentId === undefined) {
            return badRequest(res, "Invalid Parameters");
        }
        await service.associatePayment(dto);
        return success(res, "Payment associated");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
module.exports.listPerUserId = async (req, res) => {
    let id = req.params.id;
    let result = await service.listPerUserId(id);
    return success(res, result);
}