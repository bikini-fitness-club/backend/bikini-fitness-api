"use strict";

const classService = require("../service/ClassService");
const {created, badRequest, success} = require("../config/HttpResponse");

/* Class */
module.exports.createClass = async (req, res) => {
    let clazz = req.body;
    try {
        await classService.createClass(clazz);
        return created(res, "Class created");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

module.exports.listClass = async (req, res) => {
    let result = await classService.listClass();
    return success(res, result)
}

module.exports.deleteClass = async (req, res) => {
    let classId = req.params.id;
    try {
        await classService.deleteClass(classId);
        return success(res, "Class deleted");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

module.exports.updateClass = async (req, res) => {
    let clazz = req.body;
    try {
        await classService.updateClass(clazz);
        return success(res, "Class updated");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

/* Class Model */
module.exports.createClassModel = async (req, res) => {
    let classModel = req.body;
    try {
        await classService.createClassModel(classModel);
        return created(res, "Class Model created");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

module.exports.listClassModel = async (req, res) => {
    let result = await classService.listClassModel();
    return success(res, result);
}

module.exports.deleteClassModel = async (req, res) => {
    let classModelId = req.params.id;
    try {
        await classService.deleteClassModel(classModelId);
        return success(res, "Class Model deleted");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

module.exports.updateClassModel = async (req, res) => {
    let classModel = req.body;
    try {
        await classService.updateClassModel(classModel);
        return success(res, "Class Model updated");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

/* Class Type */
module.exports.createClassType = async (req, res) => {
    let classType = req.body;
    try {
        await classService.createClassType(classType);
        return created(res, "Class Type created");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

module.exports.listClassType = async (req, res) => {
    let result = await classService.listClassType();
    return success(res, result);
}

module.exports.deleteClassType = async (req, res) => {
    let classTypeId = req.params.id;
    try {
        await classService.deleteClassType(classTypeId);
        return success(res, "Class Type deleted");
    } catch (exception) {
        return badRequest(res, exception);
    }
}

module.exports.updateClassType = async (req, res) => {
    let classType = req.body;
    try {
        await classService.updateClassType(classType);
        return success(res, "Class Type updated");
    } catch (exception) {
        return badRequest(res, exception);
    }
}





