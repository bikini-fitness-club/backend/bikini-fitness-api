const {success, badRequest} = require("../config/HttpResponse");
const service = require("../service/MigrationService");

module.exports.users = async (req, res) => {
    let dto = req.body;
    try {
        await service.migrateUserAndMemberships(dto);
        return success(res, "");
    } catch (exception) {
        return badRequest(res, exception);
    }
}