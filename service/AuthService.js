"use strict";
const authRepository = require("../repository/AuthRepository");
const campusRepository = require("../repository/CampusRepository");
const userCampusRepository = require("../repository/UserCampusRepository");
const profileRepository = require("../repository/ProfileRepository");
const tokenRepository = require("../repository/TokenRepository");
const userRepository = require("../repository/UserRepository");
const unwrapStructure = require("../config/UnwrapStructure");
const uuid = require("uuid");
const emailCommand = require("../command/EmailCommand");

module.exports.validate = async (email, password) => {
    if (email.trim().length === 0 || password.trim().length === 0) {
        throw "Credentials invalid";
    }

    let result = await authRepository.validateUser(email, password);
    if (result === undefined || result === null || result.length === 0) {
        throw "Credentials not found";
    }

    let user = result[0];
    let campusUser = await userCampusRepository.findByUserId(user.id);
    if (campusUser.length > 0) {
        let locationUserList = [];
        let campus = await campusRepository.listAll();
        for (const cu of campusUser) {
            locationUserList.push(campus.filter(c => c.id === cu.campusId)[0]);
        }

        user.campus = locationUserList;
    }

    let profileResult = await profileRepository.get(user.profileId);
    if (profileResult.Item === undefined || profileResult.Item === null) {
        throw "Profile not found";
    }

    delete user.profileId;
    user.profile = unwrapStructure(profileResult.Item);
    delete user.password;

    return user;
}

module.exports.resetPassword = async (model) => {
    if (model.password === undefined || model.password.trim().length === 0
        || model.token === undefined || model.token.trim().length === 0) {

        throw "Credentials invalid";
    }

    let tokens = await tokenRepository.get(model.token);
    if (tokens.length === 0) throw "Token not found";

    let token = tokens[0];
    let resultUser = await userRepository.get(token.userId);
    if (resultUser.Item === undefined) throw "User not found";

    let user = unwrapStructure(resultUser.Item);
    if (user.status === 'I') throw "User inactive";

    user.password = model.password;
    token.status = "I";
    await userRepository.update(user);
    await tokenRepository.update(token);

}

/**
 * Method to validate the email and if it exists and is active,
 * a link is generated to change the password
 *
 * @param dto - Request Body
 * @returns {Promise<void>}
 */
module.exports.validateEmail = async (dto) => {
    let users = await userRepository.findByEmail(dto.email);
    if (users.length > 0) throw "This email already exists";
}