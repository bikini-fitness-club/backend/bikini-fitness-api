"use strict";

const moment = require("moment");
const repository = require("../repository/MembershipsRepository");
const userRepository = require("../repository/UserRepository");
const userMembershipRepository = require("../repository/UserMembershipRepository");
const emailCommand = require("../command/EmailCommand");
const uuid = require("uuid");
const tokenRepository = require("../repository/TokenRepository");
const unwrapStructure = require("../config/UnwrapStructure");
const toString = require("../config/ToString");
const validator = require("../config/Validator");

/**
 *  Method to create Membership
 *
 * @param entity
 * @returns {Promise<void>}
 */
module.exports.create = async (entity) => {
    await repository.create(toString(entity));
}

/**
 * Method to create Membership
 *
 * @returns {Promise<*[]>}
 */
module.exports.list = async () => {
    return await repository.list();
}

/**
 * Method to delete Membership
 *
 * @param id
 * @returns {Promise<void>}
 */
module.exports.delete = async (id) => {
    let membership = await repository.getItem(id);
    console.log(JSON.stringify(membership));
    if (membership === undefined) throw "Membership not found";

    membership.status = "I";
    await repository.update(toString(membership));
}

/**
 * Method to update Membership
 *
 * @param dto
 * @returns {Promise<void>}
 */
module.exports.update = async (dto) => {
    let membership = await repository.getItem(dto.id);
    if (membership === undefined) throw "Membership not found";

    membership.name = dto.name;
    membership.duration = dto.duration;
    membership.unitTime = dto.unitTime;
    membership.hasGroupClass = dto.hasGroupClass;
    membership.relationalId = dto.relationalId;
    membership.unlimitedGroupClasses = dto.unlimitedGroupClasses;
    membership.numberGroupClasses = dto.numberGroupClasses;
    membership.unitTimeGroupClasses = dto.unitTimeGroupClasses;
    membership.hasPersonalClass = dto.hasPersonalClass;
    membership.unlimitedPersonalClasses = dto.unlimitedPersonalClasses;
    membership.numberPersonalClasses = dto.numberPersonalClasses;
    membership.unitTimePersonalClasses = dto.unitTimePersonalClasses;
    membership.cost = dto.cost;

    await repository.update(toString(membership));
}

module.exports.associateUser = async (dto) => {
    if (validator.undefinedAndEmpty(dto.userId)) throw "UserId Invalid";
    if (validator.undefinedAndEmpty(dto.membershipId)) throw "MembershipId Invalid";
    if (validator.undefinedAndEmpty(dto.initialDate)) throw "Initial Date Invalid";

    let membershipResult = await repository.get(dto.membershipId);
    if (membershipResult.Item === undefined) throw "Membership not found";

    let user = await userRepository.getItem(dto.userId);
    if (user === undefined) throw "User not found";

    let membership = unwrapStructure(membershipResult.Item);
    let initialDate = moment(dto.initialDate, "YYYY-MM-DD").format("DD/MM/YYYY");
    let finalDate = moment(dto.initialDate, "YYYY-MM-DD");
    switch (membership.unitTime) {
        case "Y":
            finalDate = finalDate.add(membership.duration, 'y').format("DD/MM/YYYY");
            break;

        case "M":
            finalDate = finalDate.add(membership.duration, 'M').format("DD/MM/YYYY");
            break;

        case "W":
            finalDate = finalDate.add(membership.duration, 'w').format("DD/MM/YYYY");
            break;

        case "D":
            finalDate = finalDate.add(membership.duration, 'd').format("DD/MM/YYYY");
            break;
    }

    let model = {
        "userId": dto.userId,
        "membershipId": membership.id,
        "initialDate": initialDate,
        "finalDate": finalDate
    }

    await userMembershipRepository.associate(model);
    await emailCommand.sendPaymentEmail(user.email);
}

/**
 * Method to associate a Payment ID with a user's membership
 *
 * @param dto
 * @returns {Promise<void>}
 */
module.exports.associatePayment = async (dto) => {
    let userMembership = await userMembershipRepository.getItem(dto.userMembershipId);
    if (userMembership === undefined) throw "The user does not have a membership associated";

    let user = await userRepository.getItem(userMembership.userId);
    if (user === undefined) throw "User not found";

    let membership = await repository.getItem(userMembership.membershipId);
    if (membership === undefined) throw "Membership not found";

    userMembership.paymentId = dto.paymentId;
    let userId = user.id;
    let token = uuid.v4().replaceAll("-", '');

    await userMembershipRepository.update(userMembership);
    await tokenRepository.create({userId, token});
    await emailCommand.sendEmail(user.email, membership.name, token);

}

/**
 * List of memberships assigned to the user
 *
 * @returns {Promise<*[]>}
 * @param userId
 */
module.exports.listPerUserId = async (userId) => {
    let user = await userRepository.getItem(userId);
    if (user === undefined) throw "User not found";
    if (user.status === "I") throw "User inactive";

    let userMembershipList = await userMembershipRepository.findAllByUserId(user.id);
    if (userMembershipList.length === 0) return [];

    let membershipList = await repository.listAll();
    for (let userMember of userMembershipList) {
        let membership = membershipList.filter(m => m.id === userMember.membershipId)[0];
        userMember.membershipName = membership === undefined ? "" : membership.name;
    }

    return userMembershipList;

}