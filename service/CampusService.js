"use strict";

const repository = require("../repository/CampusRepository");
const userRepository = require("../repository/UserRepository");
const userCampusRepository = require("../repository/UserCampusRepository");
const unwrapStructure = require("../config/UnwrapStructure");

/**
 * Return the campus list.
 *
 * @author Anthony Cajamarca
 * @returns {Promise<*[]>}
 */
module.exports.list = async () => {
    return await repository.list();
};

/**
 * Service to update the campus.
 *
 * @author Anthony Cajamarca
 * @param params
 * @returns {Promise<void>}
 */
module.exports.update = async (params) => {
    let campus = await repository.get(params.id);
    if (campus.Item === undefined || campus.Item === null) {
        throw "Campus not found";
    }

    await repository.update(params);
}

/**
 * Service to delete the campus.
 *
 * @author Anthony Cajamarca
 * @param campusId
 * @returns {Promise<void>}
 */
module.exports.delete = async (campusId) => {
    let campusEntity = await repository.get(campusId);
    if (campusEntity.Item === undefined || campusEntity.Item === null) {
        throw "Campus not found";
    }

    let campus = unwrapStructure(campusEntity.Item);
    campus.status = "I";
    await repository.update(campus);
}

/**
 * Service to create the campus.
 *
 * @author Anthony Cajamarca
 * @param campus
 * @returns {Promise<void>}
 */
module.exports.create = async (campus) => {
    await repository.create(campus);
}

/**
 *
 * @param userId
 * @returns {Promise<*[]>}
 */
module.exports.getByUserId = async (userId) => {
    let user = await userRepository.getItem(userId);
    if (user === undefined) throw "User not found";

    let campusUserList = await userCampusRepository.findByUserId(user.id);
    let campusUser = [];
    if (campusUserList.length > 0) {
        let campus = await repository.list();
        for (const item of campusUserList) {
            campusUser.push(campus.filter(c => c.id === item.campusId)[0]);
        }
    }

    return campusUser;
}
