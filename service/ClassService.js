"use strict";

const classRepository = require("../repository/ClassRepository");
const unwrapStructure = require("../config/UnwrapStructure");
const toString = require("../config/ToString");

module.exports.createClassModel = async (classModel) => {
    await classRepository.createClassModel(classModel);
}

module.exports.createClassType = async (classType) => {
    await classRepository.createClassType(classType);
}

module.exports.createClass = async (clazz) => {
    await classRepository.createClass(toString(clazz));
}

module.exports.listClass = async () => {
    return await classRepository.listClass();
}

module.exports.listClassModel = async () => {
    return await classRepository.listClassModel();
}

module.exports.listClassType = async () => {
    return await classRepository.listClassType();
}

module.exports.deleteClass = async (classId) => {
    let clazzEntity = await classRepository.get(classId, "classes");
    if (clazzEntity.Item === undefined || clazzEntity.Item === null) {
        throw "Class not found";
    }

    let clazz = unwrapStructure(clazzEntity.Item);
    clazz.status = "I";
    await classRepository.updateClass(clazz);
}

module.exports.deleteClassModel = async (classModelId) => {
    let classModelEntity = await classRepository.get(classModelId, "classModels");
    if (classModelEntity.Item === undefined || classModelEntity.Item === null) {
        throw "ClassModel not found";
    }

    let classModel = unwrapStructure(classModelEntity.Item);
    classModel.status = "I";
    await classRepository.updateClassModel(classModel);
}

module.exports.deleteClassType = async (classTypeId) => {
    let classTypeEntity = await classRepository.get(classTypeId, "classTypes");
    if (classTypeEntity.Item === undefined || classTypeEntity.Item === null) {
        throw "ClassType not found";
    }

    let classType = unwrapStructure(classTypeEntity.Item);
    classType.status = "I";
    await classRepository.updateClassType(classType);
}

module.exports.updateClass = async (clazz) => {
    let clazzEntity = await classRepository.get(clazz.id, "clasees");
    if (clazzEntity.Item === undefined || clazzEntity.Item === null) {
        throw "Class not found";
    }

    let model = unwrapStructure(clazzEntity.Item);
    model.name = clazz.name;
    model.showPersonalClassList = clazz.showPersonalClassList;

    await classRepository.updateClass(toString(model));
}

module.exports.updateClassModel = async (classModel) => {
    let classModelEntity = await classRepository.get(classModel.id, "classModels");
    if (classModelEntity.Item === undefined || classModelEntity.Item === null) {
        throw "Class Model not found";
    }

    let model = unwrapStructure(classModelEntity.Item);
    model.name = classModel.name;

    await classRepository.updateClassModel(model);
}

module.exports.updateClassType = async (classType) => {
    let classTypeEntity = await classRepository.get(classType.id, "classTypes");
    if (classTypeEntity.Item === undefined || classTypeEntity.Item === null) {
        throw "Class Type not found";
    }

    let model = unwrapStructure(classTypeEntity.Item);
    model.name = classType.name;

    await classRepository.updateClassType(model);
}

