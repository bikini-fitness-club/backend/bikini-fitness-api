"use strict";

const profileRepository = require("../repository/ProfileRepository");

module.exports.create = async (dto) => {
    await profileRepository.create(dto);
}

module.exports.list = async () => {
    return await profileRepository.list();
}

module.exports.delete = async (id) => {
    let profile = await profileRepository.getItem(id);
    if (profile === undefined) throw "Profile not found";

    try {
        profile.status = "I";
        await profileRepository.update(profile);
    } catch (e) {
        console.error(e);
    }
}

module.exports.update = async (dto) => {
    let profile = await profileRepository.getItem(dto.id);
    if (profile === undefined) throw "Profile not found";

    profile.name = dto.name;
    await profileRepository.update(profile);
}