const userRepository = require("../repository/UserRepository");

/**
 * Migración de los usuarios con membresias de la app actual.
 *
 * @param listUser
 * @returns {Promise<void>}
 */
module.exports.migrateUserAndMemberships = async (listUser) => {
    for (const user of listUser) {
        await userRepository.migrate(user);
    }
}