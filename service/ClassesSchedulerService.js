"use strict";

const repository = require("../repository/ClassesSchedulerRepository");
const classesSchedulerDetailRepository = require("../repository/ClassesSchedulerDetailRepository");
const classesSchedulerReserveRepository = require("../repository/ClassesSchedulerReserverRepository");
const classRepository = require("../repository/ClassRepository");
const userRepository = require("../repository/UserRepository");
const userMembershipRepository = require("../repository/UserMembershipRepository");
const membershipsRepository = require("../repository/MembershipsRepository");
const campusRepository = require("../repository/CampusRepository");
const classBuilder = require("../builder/ClassBuilder");
const profileEnum = require("../enum/ProfileEnum");
const moment = require("moment");
const toString = require("../config/ToString");
const format = "YYYY-MM-DD HH:mm";
const formatDate = "YYYY-MM-DD";
const formatDB = "DD/MM/YYYY";
const emailCommand = require("../command/EmailCommand");

/**
 * Create a class calendar with your scheduled classes.
 *
 * @param dto
 * @returns {Promise<*[]>}
 */
module.exports.create = async (dto) => {
  let clazz = await classRepository.getItem(dto.classId);
  if (clazz === undefined) throw "Class not found";

  let campus = await campusRepository.getItem(dto.campusId);
  if (campus === undefined) throw "Campus not found";

  let user = await userRepository.getItem(dto.userId);
  if (user === undefined) throw "User not found";

  let classSchedulerId = await repository.create(dto);
  return await createSchedule(classSchedulerId, dto, clazz, campus, user);
};

/**
 * Create the class calendar
 *
 * @param classSchedulerId
 * @param parameterClass
 * @param clazz
 * @param campus
 * @param user
 * @returns {Promise<[]>}
 */
const createSchedule = async (
  classSchedulerId,
  parameterClass,
  clazz,
  campus,
  user
) => {
  try {
    if (parameterClass.recurrent === "0") {
      return await createNonRecurringClasses(
        classSchedulerId,
        parameterClass,
        clazz,
        campus,
        user
      );
    } else {
      // To Do: Validar que por lo menos un día de la semana esté seleccionado
      return await createRecurringClasses(
        classSchedulerId,
        parameterClass,
        clazz,
        campus,
        user
      );
    }
  } catch (e) {
    console.error(e);
  }
};

/**
 * Method where the creation of non-recurring classes is carried out
 *
 * @param classSchedulerId
 * @param parameterClass
 * @param clazz
 * @param campus
 * @param user
 * @returns {Promise<*[]>}
 */
const createNonRecurringClasses = async (
  classSchedulerId,
  parameterClass,
  clazz,
  campus,
  user
) => {
  let createdClass = await classBuilder.buildClass(
    parameterClass,
    clazz,
    campus,
    parameterClass.initialDate,
    user
  );

  let result = [];
  switch (createdClass.classMode.toUpperCase()) {
    case "P":
      createdClass.classId = "";
      createdClass.className = "Personal Class";
      await classesSchedulerDetailRepository.createDetail(
        classSchedulerId,
        createdClass
      );
      break;

    case "G":
      let finalClassHour = moment(createdClass.initialHour, "HH:mm")
        .add(parseInt(createdClass.duration), "h")
        .format("HH:mm");

      let createdClassesList =
        await classesSchedulerDetailRepository.listClassPerDateAndRangeHour(
          createdClass.initialDate,
          createdClass.initialHour,
          finalClassHour
        );

      if (createdClassesList.length === 0) {
        await classesSchedulerDetailRepository.createDetail(
          classSchedulerId,
          createdClass
        );
      } else {
        result.push({ class: createdClass, isPossibleCreate: false });
      }
      break;
  }
  return result;
};

/**
 * Method to create recurring classes
 *
 * @param classSchedulerId
 * @param parameterClass
 * @param clazz
 * @param campus
 * @param user
 * @returns {Promise<*[]>}
 */
const createRecurringClasses = async (
  classSchedulerId,
  parameterClass,
  clazz,
  campus,
  user
) => {
  let totalDays = 7 * parseInt(parameterClass.numberWeeks);
  let initialDate = moment(parameterClass.initialDate);
  let finalDate = moment(parameterClass.initialDate).add(totalDays, "days");

  let result = [];
  let classesList = [];
  while (initialDate <= finalDate) {
    if (parameterClass[initialDate.format("dddd").toLowerCase()] === "1") {
      let date = initialDate.format("YYYY-MM-DD");
      let createdClass = await classBuilder.buildClass(
        parameterClass,
        clazz,
        campus,
        date,
        user
      );
      classesList.push(createdClass);
    }
    initialDate.add(1, "d");
  }

  for (const classGenerated of classesList) {
    switch (classGenerated.classMode.toUpperCase()) {
      case "P":
        classGenerated.classId = "";
        classGenerated.className = "Personal Class";
        result.push({ class: classGenerated, isPossibleCreate: true });
        break;

      case "G":
        let finalClassHour = moment(classGenerated.initialHour, "HH:mm")
          .add(parseInt(classGenerated.duration), "h")
          .format("HH:mm");

        let createdClassesList =
          await classesSchedulerDetailRepository.listClassPerDateAndRangeHour(
            classGenerated.initialDate,
            classGenerated.initialHour,
            finalClassHour,
            classGenerated.campusId
          );

        result.push({
          class: classGenerated,
          isPossibleCreate: createdClassesList.length === 0,
        });
    }
  }

  if (result.filter((c) => c.isPossibleCreate === false).length === 0) {
    for (const item of result) {
      await classesSchedulerDetailRepository.createDetail(
        classSchedulerId,
        item.class
      );
    }
    result = [];
  }

  return result;
};

/**
 * List of scheduled classes
 *
 * @returns {Promise<*[]>}
 */
module.exports.list = async () => {
  let users = await userRepository.list("1bfb7d96-1f4d-4a13-bb96-3d24a17baba2");
  let classes = await classRepository.listClass();

  let scheduledClasses = await repository.list();
  let items = [];
  for (let clazz of scheduledClasses) {
    let user = users.filter((u) => u.id === clazz.userId);
    let classList = classes.filter((c) => c.id === clazz.classId);

    clazz.className = classList[0].name;
    clazz.userName = user[0].firstName + " " + user[0].lastName;

    items.push(clazz);
  }

  return items;
};

/**
 * List of scheduled classes
 *
 * @returns {Promise<*[]>}
 */
module.exports.listClassDetail = async (
  classId,
  trainerId,
  locationId,
  classType,
  date
) => {
  const format = "YYYY-MM-DD HH:mm";
  let classList = (
    await classesSchedulerDetailRepository.listClassDetail(
      classId,
      trainerId,
      locationId,
      classType,
      date
    )
  ).map((p) => {
    let date = p.initialDate + " " + p.initialHour;
    let initialDate = moment(date, format).format(format);
    let finalDate = moment(date, format)
      .add(parseInt(p.duration), "h")
      .format(format);

    p.start = initialDate;
    p.end = finalDate;
    return p;
  });

  return classList.sort((a, b) => {
    let p1 = a.initialDate + " " + a.initialHour;
    let p2 = b.initialDate + " " + b.initialHour;
    return moment(p1, format).unix() - moment(p2, format).unix();
  });
};

/**
 * Elimination of scheduled classes
 *
 * @param id
 * @returns {Promise<void>}
 */
module.exports.delete = async (id) => {
  let classSchedule = await classesSchedulerDetailRepository.getItem(id);
  if (classSchedule === undefined) throw "Class not found";

  await classesSchedulerDetailRepository.delete(id);
};

/**
 * Exposed service to update a scheduled class.
 *
 * @param dto
 * @returns {Promise<void>}
 */
module.exports.update = async (dto) => {
  let classScheduleDetail = await classesSchedulerDetailRepository.getItem(
    dto.id
  );
  if (classScheduleDetail === undefined) throw "Class Schedule not found";
  if (dto.userLimit < classScheduleDetail.participants)
    throw "The class cannot have one space less than registered participants";

  try {
    classScheduleDetail.initialDate = dto.initialDate;
    classScheduleDetail.initialHour = dto.initialHour;
    classScheduleDetail.userLimit = dto.userLimit;
    classScheduleDetail = toString(classScheduleDetail);

    await classesSchedulerDetailRepository.updateDetail(classScheduleDetail);
  } catch (e) {
    console.error(e);
  }
};

/**
 *
 * @param userId
 * @param campusId
 * @returns {Promise<{groupClass: *[], personalClass: *[]}>}
 */
module.exports.classSchedulerPerUser = async (userId, campusId) => {
  let user = await userRepository.getItem(userId);
  if (user === undefined) throw "User not found";

  let campus = await campusRepository.getItem(campusId);
  if (campus === undefined) throw "Campus not found";

  switch (user.profileId) {
    case profileEnum.client():
      return await getClientClasses(user, campus);

    case profileEnum.trainer():
      return await getTrainerClasses(user, campus);

    default:
      throw "User profile is not accepted";
  }
};

const getClientClasses = async (user, campus) => {
  let userMembershipList = await userMembershipRepository.findByUserId(user.id);
  if (userMembershipList === undefined)
    throw "User without assigned membership";
  const userMembership = userMembershipList[0];

  let membership = await membershipsRepository.getItem(
    userMembership.membershipId
  );
  if (membership === undefined) throw "Membership not found";

  let personalClassList = [];
  const format = "YYYY-MM-DD HH:mm";
  if (membership.hasPersonalClass === 1) {
    personalClassList = (
      await classesSchedulerDetailRepository.listPersonalClass(
        campus.id,
        userMembership.initialDate,
        userMembership.finalDate
      )
    ).map((p) => {
      let date = p.initialDate + " " + p.initialHour;
      let initialDate = moment(date, format).format(format);
      let finalDate = moment(date, format)
        .add(parseInt(p.duration), "h")
        .format(format);

      return {
        id: p.id,
        classId: p.classId,
        title: p.className,
        trainer: p.userName,
        start: initialDate,
        end: finalDate,
        userLimit: p.userLimit,
        participants: p.participants,
        campusName: p.campusName,
        description: p.description,
      };
    });
  }

  let groupClassList = [];
  if (membership.hasGroupClass === 1) {
    let list = await classesSchedulerDetailRepository.listGroupClass(
      campus.id,
      userMembership.initialDate,
      userMembership.finalDate
    );

    groupClassList = list.map((i) => {
      let date = i.initialDate + " " + i.initialHour;
      let initialDate = moment(date, format).format(format);
      let finalDate = moment(date, format)
        .add(parseInt(i.duration), "h")
        .format(format);

      return {
        id: i.id,
        classId: i.classId,
        title: i.className,
        trainer: i.userName,
        start: initialDate,
        end: finalDate,
        userLimit: i.userLimit,
        participants: i.participants,
        campusName: i.campusName,
        description: i.description,
      };
    });
  }

  return {
    personalClass: personalClassList,
    groupClass: groupClassList,
  };
};

const getTrainerClasses = async (user, campus) => {
  let classes =
    await classesSchedulerDetailRepository.listClassPerUserIdAndCampusId(
      user.id,
      campus.id
    );

  let result = classes.map((p) => {
    let date = p.initialDate + " " + p.initialHour;
    let initialDate = moment(date, format).format(format);
    let finalDate = moment(date, format)
      .add(parseInt(p.duration), "h")
      .format(format);

    return {
      id: p.id,
      classId: p.classId,
      title: p.className,
      trainer: p.userName,
      classMode: p.classMode,
      start: initialDate,
      end: finalDate,
      userLimit: p.userLimit,
      participants: p.participants,
      campusName: p.campusName,
      description: p.description,
    };
  });

  return {
    personalClass: result
      .filter((p) => p.classMode === "P")
      .map((p) => {
        delete p.classMode;
        return p;
      }),
    groupClass: result
      .filter((p) => p.classMode === "G")
      .map((p) => {
        delete p.classMode;
        return p;
      }),
  };
};

/**
 *
 * @param dto
 * @returns {Promise<void>}
 */
module.exports.registerUserClassesScheduler = async (dto) => {
  let user = await userRepository.getItem(dto.userId);
  if (user === undefined) throw "User not found";

  let classSchedule = await classesSchedulerDetailRepository.getItem(
    dto.classSchedulerId
  );
  if (classSchedule === undefined) throw "Schedule Class not found";
  if (classSchedule.classMode === "P" && classSchedule.participants === 1)
    throw "Class not available";

  let userMemberships = await userMembershipRepository.findByUserId(user.id);
  if (userMemberships.length === 0) throw "User without assigned membership";
  const userMembership = userMemberships[0];

  let membership = await membershipsRepository.getItem(
    userMembership.membershipId
  );
  if (membership === undefined) throw "Membership not found";

  let initialDate = moment(userMemberships[0].initialDate, "DD/MM/YYYY").format(
    formatDate
  );
  let finalDate = moment(userMemberships[0].finalDate, "DD/MM/YYYY").format(
    formatDate
  );
  let classDate = moment(classSchedule.initialDate, "DD/MM/YYYY").format(
    formatDate
  );

  let now = moment().format(formatDate);
  if (now < initialDate) throw "Your membership is not yet active";

  if (classDate > finalDate)
    throw "The scheduled class is outside the range of your membership.";

  let reservedClasses =
    await classesSchedulerReserveRepository.getByClassScheduleIdAndUserId(
      classSchedule.id,
      user.id
    );
  if (reservedClasses.length > 0)
    throw "You are already registered in this class";

  switch (classSchedule.classMode.toUpperCase()) {
    case "P":
      await processPersonalClass(
        classSchedule,
        dto.classId,
        user.id,
        membership,
        userMembership
      );
      break;

    case "G":
      await processGroupClass(
        classSchedule,
        user.id,
        membership,
        userMembership
      );
      break;
  }

  const name = user.firstName + " " + user.lastName;
  const date = classSchedule.initialDate + " " + classSchedule.initialHour;
  await emailCommand.reserveClass(
    user.email,
    name,
    classSchedule.className,
    date
  );
};

const processPersonalClass = async (
  classSchedule,
  classId,
  userId,
  membership,
  userMembership
) => {
  if (classId === undefined || classId.trim().length === 0)
    throw "Class not found";
  if (membership.hasPersonalClass === 0)
    throw "Your membership does not have access to personalized classes";
  if (membership.unlimitedGroupClasses === 1) {
    await createClass(classSchedule, classId, userId);
  } else {
    switch (membership.unitTimePersonalClasses.toUpperCase()) {
      case "D":
        let reservedClassesDayList =
          await classesSchedulerReserveRepository.getByDateAndUser(
            classSchedule.initialDate,
            userId
          );
        if (reservedClassesDayList.length >= membership.numberPersonalClasses) {
          throw "You have already reached the maximum number of classes allowed by the membership";
        }
        await createClass(classSchedule, classId, userId);
        classSchedule.participants = classSchedule.participants + 1;
        await classesSchedulerDetailRepository.updateDetail(
          toString(classSchedule)
        );
        break;

      case "W":
      case "M":
      case "Y":
        let initialDate = moment(userMembership.initialDate, formatDB).format(
          formatDate
        );
        let finalDate = moment(userMembership.finalDate, formatDB).format(
          formatDate
        );
        let reservedClassesList =
          await classesSchedulerReserveRepository.getByDateRangeAndUserId(
            initialDate,
            finalDate,
            userId
          );
        if (reservedClassesList.length >= membership.numberPersonalClasses) {
          throw "You have already reached the maximum number of classes allowed by the membership";
        }
        await createClass(classSchedule, classId, userId);
        classSchedule.participants = classSchedule.participants + 1;
        await classesSchedulerDetailRepository.updateDetail(
          toString(classSchedule)
        );
        break;

      /*case "M":
                let year = moment(classSchedule.initialDate, formatDate).year();
                let month = moment(classSchedule.initialDate, formatDate).month();
                let initialDateMonth = moment([year, month, 1]).format(formatDate);
                let finalDateMonth = moment([year, month, 31]).format(formatDate);
                let reservedClassesMonthList = await classesSchedulerReserveRepository.getByDateRangeAndUserId(initialDateMonth, finalDateMonth, userId);
                if (reservedClassesMonthList.length >= membership.numberPersonalClasses) {
                    throw "You have already reached the maximum number of classes allowed by the membership";
                }
                await createClass(classSchedule, classId, userId);
                classSchedule.participants = classSchedule.participants + 1;
                await classesSchedulerDetailRepository.updateDetail(toString(classSchedule));
                break;

            case "Y":
                let yearDate = moment(classSchedule.initialDate, formatDate).year();
                let initialDateYear = moment([yearDate, 0, 1]).format(formatDate);
                let finalDateYear = moment([yearDate, 11, 31]).format(formatDate);
                let reservedClassYear = await classesSchedulerReserveRepository.getByDateRangeAndUserId(initialDateYear, finalDateYear, userId);
                if (reservedClassYear.length >= membership.numberPersonalClasses) {
                    throw "You have already reached the maximum number of classes allowed by the membership";
                }
                await createClass(classSchedule, classId, userId);
                classSchedule.participants = classSchedule.participants + 1;
                await classesSchedulerDetailRepository.updateDetail(toString(classSchedule));
                break;*/
    }
  }
};

const processGroupClass = async (
  classSchedule,
  userId,
  membership,
  userMembership
) => {
  if (membership.hasGroupClass === 0)
    throw "Your membership does not have access to group classes";
  if (classSchedule.participants >= classSchedule.userLimit)
    throw "The selected class has no availability";
  if (membership.unlimitedGroupClasses === 1) {
    await createClass(classSchedule, classSchedule.classId, userId);
    classSchedule.participants = classSchedule.participants + 1;
    await classesSchedulerDetailRepository.updateDetail(
      toString(classSchedule)
    );
  } else {
    switch (membership.unitTimeGroupClasses.toUpperCase()) {
      case "D":
        let reservedClassesDayList =
          await classesSchedulerReserveRepository.getByDateAndUser(
            classSchedule.initialDate,
            userId
          );
        if (reservedClassesDayList.length >= membership.numberGroupClasses) {
          throw "You have already reached the maximum number of classes allowed by the membership";
        }
        await createClass(classSchedule, classSchedule.classId, userId);
        classSchedule.participants = classSchedule.participants + 1;
        await classesSchedulerDetailRepository.updateDetail(
          toString(classSchedule)
        );
        break;

      case "W":
      case "M":
      case "Y":
        let initialDate = moment(userMembership.initialDate, formatDB).format(
          formatDate
        );
        let finalDate = moment(userMembership.finalDate, formatDB).format(
          formatDate
        );
        let reservedClassesList =
          await classesSchedulerReserveRepository.getByDateRangeAndUserId(
            initialDate,
            finalDate,
            userId
          );
        if (reservedClassesList.length >= membership.numberGroupClasses) {
          throw "You have already reached the maximum number of classes allowed by the membership";
        }
        await createClass(classSchedule, classSchedule.classId, userId);
        classSchedule.participants = classSchedule.participants + 1;
        await classesSchedulerDetailRepository.updateDetail(
          toString(classSchedule)
        );
        break;

      /*case "M":
                let year = moment(classSchedule.initialDate, formatDate).year();
                let month = moment(classSchedule.initialDate, formatDate).month();
                let initialDateMonth = moment([year, month, 1]).format(formatDate);
                let finalDateMonth = moment([year, month, 31]).format(formatDate);
                let reservedClassesMonthList = await classesSchedulerReserveRepository.getByDateRangeAndUserId(initialDateMonth, finalDateMonth, userId);
                if (reservedClassesMonthList.length >= membership.numberPersonalClasses) {
                    throw "You have already reached the maximum number of classes allowed by the membership";
                }
                await createClass(classSchedule, classSchedule.classId, userId);
                classSchedule.participants = classSchedule.participants + 1;
                await classesSchedulerDetailRepository.updateDetail(toString(classSchedule));
                break;*/

      /*case "Y":
                let yearDate = moment(classSchedule.initialDate, formatDate).year();
                let initialDateYear = moment([yearDate, 0, 1]).format(formatDate);
                let finalDateYear = moment([yearDate, 11, 31]).format(formatDate);
                let reservedClassYear = await classesSchedulerReserveRepository.getByDateRangeAndUserId(initialDateYear, finalDateYear, userId);
                if (reservedClassYear.length >= membership.numberPersonalClasses) {
                    throw "You have already reached the maximum number of classes allowed by the membership";
                }
                await createClass(classSchedule, classSchedule.classId, userId);
                classSchedule.participants = classSchedule.participants + 1;
                await classesSchedulerDetailRepository.updateDetail(toString(classSchedule));
                break;*/
    }
  }
};

const createClass = async (classSchedule, classId, userId) => {
  await classesSchedulerReserveRepository.create({
    classScheduleId: classSchedule.id,
    campusId: classSchedule.campusId,
    classId: classId,
    classMode: classSchedule.classMode,
    userId: userId,
    trainerId: classSchedule.userId,
    initialDate: classSchedule.initialDate,
    initialHour: classSchedule.initialHour,
    duration: classSchedule.duration,
  });
};

/**
 * Method for list reserved classes per UserId
 *
 * @param userId
 * @param campusId
 * @returns {Promise<*[]>}
 */
module.exports.listUserClassesSchedulerReserved = async (userId, campusId) => {
  let userClasses = [];

  let user = await userRepository.getItem(userId);
  if (user === undefined) throw "User not found";

  try {
    let classReservedList =
      await classesSchedulerReserveRepository.getPerUserId(userId, campusId);
    if (classReservedList.length > 0) {
      let trainers = await userRepository.list(profileEnum.trainer());
      let campus = await campusRepository.listAll();
      let classes = await classRepository.listAll();

      for (const classReserve of classReservedList) {
        let classSchedule = await classesSchedulerDetailRepository.getItem(
          classReserve.classScheduleId
        );
        let trainerUser = trainers.filter(
          (t) => t.id === classReserve.trainerId
        )[0];
        const nameTrainer =
          trainerUser === undefined
            ? ""
            : trainerUser.firstName + " " + trainerUser.lastName;

        let date = classReserve.initialDate + " " + classReserve.initialHour;
        let initialDate = moment(date, format).format(format);
        let finalDate = moment(date, format)
          .add(parseInt(classReserve.duration), "h")
          .format(format);

        userClasses.push({
          id: classReserve.id,
          classId: classReserve.classId,
          title: classes.filter((c) => c.id === classReserve.classId)[0].name,
          trainer: nameTrainer,
          start: initialDate,
          end: finalDate,
          campusName: campus.filter((c) => c.id === classReserve.campusId)[0]
            .name,
          classMode: classReserve.classMode,
          initialDate: classReserve.initialDate,
          initialHour: classReserve.initialHour,
          duration: classReserve.duration,
          description: classSchedule.description,
        });
      }
    }
  } catch (e) {
    console.error(JSON.stringify(e));
    console.error(e);
    throw "Error querying the user's reserved classes";
  }

  return userClasses;
};

/**
 * Get detail class per Class Schedule Id
 *
 * @param id
 * @returns {Promise<{scheduleClass, participants: *[]}>}
 */
module.exports.getClassesScheduler = async (id) => {
  let scheduleClass = await classesSchedulerDetailRepository.getItem(id);
  if (scheduleClass === undefined) throw "Class Not found";

  let users = [];
  let classSchedulerReserveList =
    await classesSchedulerReserveRepository.getPerClassScheduleId(
      scheduleClass.id
    );

  for (const classUser of classSchedulerReserveList) {
    let user = await userRepository.getItem(classUser.userId);
    if (user !== undefined) {
      delete user.password;
      delete user.address;
      delete user.mobile;
      users.push(user);
    }
  }

  return {
    scheduleClass: scheduleClass,
    participants: users,
  };
};

/**
 *
 * @param campusId
 * @returns {Promise<*[]>}
 */
module.exports.getClassesSchedulerFreeClass = async (campusId) => {
  let classes = await classesSchedulerDetailRepository.listClassPerLocationId(
    campusId
  );
  const format = "YYYY-MM-DD HH:mm";

  return classes.map((p) => {
    let date = p.initialDate + " " + p.initialHour;
    let initialDate = moment(date, format).format(format);
    let finalDate = moment(date, format)
      .add(parseInt(p.duration), "h")
      .format(format);

    return {
      id: p.id,
      classId: p.classId,
      title: p.className,
      trainer: p.userName,
      start: initialDate,
      end: finalDate,
      userLimit: p.userLimit,
      participants: p.participants,
      campusName: p.campusName,
    };
  });
};

/**
 *
 * @param userId
 * @param classScheduleReserveId
 * @returns {Promise<{allowed: boolean}>}
 */
module.exports.validateDeleteUserClassesSchedulerReserve = async (
  userId,
  classScheduleReserveId
) => {
  let user = await userRepository.getItem(userId);
  if (user === undefined) throw "User not found";

  let classReservedList = await classesSchedulerReserveRepository.getById(
    classScheduleReserveId
  );
  if (classReservedList.length === 0) throw "Class Reserved not found";
  let classReserved = classReservedList[0];
  if (classReserved.userId !== user.id)
    throw "User does not belong to the selected class";

  //if (classReserved.classMode === 'P') return {allowed: true}

  let classDate = moment(
    classReserved.initialDate + " " + classReserved.initialHour
  ).format(format);
  let hours = moment().diff(classDate, "hours");
  let allowed = hours <= -4;

  return { allowed };
};

/**
 *
 * @param userId
 * @param classScheduleReserveId
 * @returns {Promise<void>}
 */
module.exports.deleteUserClassesSchedulerReserve = async (
  userId,
  classScheduleReserveId
) => {
  let user = await userRepository.getItem(userId);
  if (user === undefined) throw "User not found";

  let classReservedList = await classesSchedulerReserveRepository.getById(
    classScheduleReserveId
  );
  if (classReservedList.length === 0) throw "Class Reserved not found";
  let classReserved = classReservedList[0];
  if (classReserved.userId !== user.id)
    throw "User does not belong to the selected class";

  let classSchedule = await classesSchedulerDetailRepository.getItem(
    classReserved.classScheduleId
  );
  classSchedule.participants = classSchedule.participants - 1;

  await classesSchedulerReserveRepository.delete(classReserved.id);
  await classesSchedulerDetailRepository.updateDetail(toString(classSchedule));

  const userName = user.firstName + " " + user.lastName;
  const date = classSchedule.initialDate + " " + classSchedule.initialHour;
  await emailCommand.cancelClass(
    user.email,
    userName,
    classSchedule.className,
    date
  );
};
