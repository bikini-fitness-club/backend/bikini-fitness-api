"use strict";
const classesSchedulerDetailRepository = require("../repository/ClassesSchedulerDetailRepository");
const classesSchedulerReserveRepository = require("../repository/ClassesSchedulerReserverRepository");
const userRepository = require("../repository/UserRepository");
const campusRepository = require("../repository/CampusRepository");
const membershipsRepository = require("../repository/MembershipsRepository");
const userMembershipRepository = require("../repository/UserMembershipRepository");
const userCampusRepository = require("../repository/UserCampusRepository");
const classesFreeRepository = require("../repository/ClassesFreeRepository");
const profileRepository = require("../repository/ProfileRepository");
const tokenRepository = require("../repository/TokenRepository");
const passwordHelper = require("../config/PasswordHelper");
const emailCommand = require("../command/EmailCommand");
const unwrapStructure = require("../config/UnwrapStructure");
const toString = require("../config/ToString");
const uuid = require("uuid");
const moment = require("moment/moment");

/**
 *
 *
 * @param user
 * @param campusName
 * @param membershipRelationalId
 * @returns {Promise<void>}
 */
module.exports.register = async (user, campusName, membershipRelationalId) => {
  try {
    let membershipList = await membershipsRepository.findByRelationId(membershipRelationalId);
    if (membershipList.length === 0) throw "Membership sent from Shopify not found";
    let membership = membershipList[0];
    let userList = await userRepository.findByEmail(user.email);

    let userID = "";
    if (userList.length > 0) {
      userID = userList[0].id;
    } else {
      user.password = await passwordHelper.generate();
      userID = await userRepository.register(user);
    }

    let campusList = [];
    if (campusName === "MSHIP-ALL-LOCATION") {
      campusList = await campusRepository.listIds();
    } else {
      campusList = await campusRepository.getBySKU(campusName);
    }

    if (campusList.length === 0) throw "Campus sent from Shopify not found";

    let finalDate;
    switch (membership.unitTime) {
      case "Y":
        finalDate = moment().add(membership.duration, "Y").format("DD/MM/YYYY");
        break;

      case "M":
        finalDate = moment().add(membership.duration, "M").format("DD/MM/YYYY");
        break;

      case "D":
        finalDate = moment().add(membership.duration, "d").format("DD/MM/YYYY");
        break;

      case "W":
        finalDate = moment().add(membership.duration, "w").format("DD/MM/YYYY");
        break;
    }

    const addUserCampus = async (campusId) => {
      await userCampusRepository.create({ userId: userID, campusId });
    };

    for (const item of campusList) {
      await addUserCampus(item.id);
    }

    await userMembershipRepository.associate({
      userId: userID,
      membershipId: membership.id,
      initialDate: moment().format("DD/MM/YYYY"),
      finalDate: finalDate,
    });

    let token = uuid.v4().replaceAll("-", "");
    await tokenRepository.create({ userId: userID, token });
    await emailCommand.sendEmail(user.email, membership.name, token);

    console.log("Shopify membership processed successfully!");
  } catch (error) {
    console.error("Error processing Shopify membership:", error);

  }
};

/**
 *
 * @param dto
 * @returns {Promise<void>}
 */
module.exports.create = async (dto) => {
  let users = await userRepository.findByEmail(dto.email);
  if (users.length > 0) throw "The sent email has already been registered";

  let profile = await profileRepository.getItem(dto.profileId);
  if (profile === undefined) throw "Profile not found";
  profile.name = profile.name.toLowerCase();

  let userId;
  let token;
  dto.status = "A";
  switch (profile.name) {
    case "admin":
      await userRepository.create(dto);
      break;

    case "client":
      userId = await userRepository.create(dto);
      for (const id of dto.campus) {
        await userCampusRepository.create({ userId: userId, campusId: id });
      }
      token = uuid.v4().replaceAll("-", "");
      await tokenRepository.create({ userId, token });
      await emailCommand.sendEmailClient(dto.email, token, dto.firstName);
      break;

    case "lead":
      userId = await userRepository.create(dto);
      for (const id of dto.campus) {
        await userCampusRepository.create({ userId: userId, campusId: id });
      }
      //await emailCommand.sendPaymentEmail(dto.email);
      break;

    case "trainer":
      userId = await userRepository.create(dto);
      for (const id of dto.campus) {
        await userCampusRepository.create({ userId: userId, campusId: id });
      }
      token = uuid.v4().replaceAll("-", "");
      await tokenRepository.create({ userId, token });
      await emailCommand.sendEmailTrainer(dto.email, token, dto.firstName);
      break;
  }
};

/**
 *
 *
 * @param id
 * @returns {Promise<void>}
 */
module.exports.delete = async (id) => {
  let result = await userRepository.get(id);
  if (result.Item === undefined || result.Item === null) {
    throw "User not found";
  }

  let model = unwrapStructure(result.Item);
  model.status = "I";
  await userRepository.update(model);
};

/**
 * Return the user list
 *
 * @param profileId
 * @returns {Promise<*[]>}
 */
module.exports.list = async (profileId) => {
  let users = await userRepository.list(profileId);
  let campus = await campusRepository.listAll();

  if (users.length > 0) {
    let profile = await profileRepository.getItem(profileId);
    let profileName = "";
    if (profile !== undefined) {
      profileName = profile.name;
    }

    let membershipList = await membershipsRepository.list();
    for (const user of users) {
      user.profileName = profileName;
      let userMembershipList = await userMembershipRepository.findByUserId(user.id);
      let userCampusList = await userCampusRepository.findByUserId(user.id);

      if (userMembershipList.length > 0) {
        let userMembership = userMembershipList[0];
        let membershipId = userMembership.membershipId;
        let membershipActive = membershipList.filter((m) => m.id === membershipId)[0];

        user.membership = {
          id: membershipActive.id,
          name: membershipActive.name,
          initialDate: userMembership.initialDate,
          finalDate: userMembership.finalDate,
          userMembershipId: userMembership.id,
          paymentId: userMembership.paymentId,
        };
      } else {
        user.membership = {
          id: "",
          name: "No membership",
          initialDate: "",
          finalDate: "",
          userMembershipId: "",
          paymentId: "",
        };
      }

      let campusPerUser = [];
      if (userCampusList.length > 0) {
        for (const userCampus of userCampusList) {
          let locations = campus.filter((c) => c.id === userCampus.campusId);
          if (locations.length > 0) campusPerUser.push(locations[0]);
        }
      }

      user.campus = campusPerUser;
      delete user.password;
      delete user.mobile;
    }
  }

  return users;
};

module.exports.update = async (dto) => {
  let user = await userRepository.getItem(dto.id);
  if (user === undefined) throw "User not found";

  user.firstName = dto.firstName;
  user.lastName = dto.lastName;
  user.email = dto.email;
  user.address = dto.address;
  user.phone = dto.phone;
  user.campusId = dto.campusId;
  user.profileId = dto.profileId;

  await userRepository.update(user);
};

/**
 * Service to validate that an email is not registered
 * and create free class
 *
 * @param dto
 * @returns {Promise<void>}
 */
module.exports.createUserFreeClass = async (dto) => {
  let users = await userRepository.findAllByEmail(dto.user.email);
  if (users.length > 0) throw "This email already exists";

  let classSchedule = await classesSchedulerDetailRepository.getItem(
    dto.class.classSchedulerId
  );
  if (classSchedule === undefined) throw "Class not found";
  if (classSchedule.status === "I") throw "Class inactive";
  if (
    classSchedule.classMode === "G" &&
    classSchedule.participants === classSchedule.userLimit
  )
    throw "Class not available";

  let classId;
  if (classSchedule.classMode === "G") {
    classId = classSchedule.classId;
  } else {
    classId =
      dto.class.classId === "" ? classSchedule.classId : dto.class.classId;
  }

  try {
    let userId = await userRepository.create({
      firstName: dto.user.firstName,
      lastName: dto.user.lastName,
      email: dto.user.email,
      address: "",
      phone: dto.user.phone,
      password: "",
      profileId: "25ab16d7-9b38-46a4-a16f-b3c85454db08",
      status: "T",
    });

    await classesSchedulerReserveRepository.create({
      classScheduleId: classSchedule.id,
      campusId: classSchedule.campusId,
      classId: classId,
      classMode: classSchedule.classMode,
      userId: userId,
      trainerId: classSchedule.userId,
      initialDate: classSchedule.initialDate,
      initialHour: classSchedule.initialHour,
    });

    classSchedule.participants = parseInt(classSchedule.participants) + 1;
    await classesSchedulerDetailRepository.updateDetail(
      toString(classSchedule)
    );
    await classesFreeRepository.create({
      userId: userId,
      email: dto.user.email,
      classScheduleId: classSchedule.id,
    });
  } catch (e) {
    console.error("Failed processing free class -> " + e);
    throw "Error creating free class";
  }
};
