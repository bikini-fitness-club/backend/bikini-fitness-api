"use strict";

const userRepository = require("../repository/UserRepository");
const membershipsRepository = require("../repository/MembershipsRepository");
const userMembershipRepository = require("../repository/UserMembershipRepository");
const ClassesSchedulerReserveRepository = require("../repository/ClassesSchedulerReserverRepository");
const userCampusRepository = require("../repository/UserCampusRepository");
const campusRepository = require("../repository/CampusRepository");
const unwrapStructure = require("../config/UnwrapStructure");
const moment = require("moment");

module.exports.generate = async (userId) => {
    let user = await userRepository.getItem(userId);
    if (user === undefined) throw "User not found"; //throw new UserNotFoundException();
    delete user.password;

    let userMembershipsList = await userMembershipRepository.findByUserId(user.id);
    if (userMembershipsList.length === 0) throw "User without assigned membership";
    let userMembership = userMembershipsList[0];

    let membershipsList = await membershipsRepository.listAll();
    if (membershipsList.length === 0) throw "Memberships not found";

    let membershipResult = await membershipsRepository.get(userMembership.membershipId);
    if (membershipResult.Item === undefined) throw "Membership not found";
    let membership = unwrapStructure(membershipResult.Item);

    try {
        let campusList = await campusRepository.listAll();
        let userCampusList = await userCampusRepository.findByUserId(user.id);
        let campus = [];
        if (userCampusList.length > 0) {
            for (const userCampus of userCampusList) {
                let locations = campusList.filter(c => c.id === userCampus.campusId);
                if (locations.length > 0) campus.push(locations[0]);
            }
        }

        /* Classes  */
        let usedClasses = 0;
        let scheduleClasses = 0;
        let totalClasses = 0;

        let today = moment().format("YYYY-MM-DD");
        let usedClassesList = await ClassesSchedulerReserveRepository.overdueClassesPerUser(user.id, today);
        let scheduledClassesList = await ClassesSchedulerReserveRepository.getPerUserId(user.id);
        if (scheduledClassesList.length > 0) {
            usedClasses = usedClassesList.length;
            scheduleClasses = scheduledClassesList.length - usedClassesList.length;
            totalClasses = scheduledClassesList.length;
        }

        let classSummary = {usedClasses, scheduleClasses, totalClasses}

        /* Memberships */
        let userMemberships = [];
        let userMembershipsList = await userMembershipRepository.findAllByUserId(user.id);
        if (userMembershipsList.length > 0) {
            for (const userMembership of userMembershipsList) {
                let membership = membershipsList.filter(m => m.id === userMembership.membershipId)[0];

                userMemberships.push({
                    name: membership.name,
                    initialDate: userMembership.initialDate,
                    finalDate: userMembership.finalDate,
                    status: userMembership.status
                });
            }
        }

        return {user, membership, userMembership, campus, classSummary, userMemberships};
    } catch (e) {
        console.error(e);
    }
}