"use strict";
const sgMail = require("@sendgrid/mail");
const properties = require("../config/Properties");

sgMail.setApiKey(
  "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"
);

const emails = [
  "florezmendezvalentina4@gmail.com",
  "florezmendezvalentina7@gmail.com",
  "valentianflorezmendez3@gmail.com",
  "valentina.florez15@gmail.com",
  "valentinafm1999@gmail.com",
  "valentinaflorezmendez0@gmail.com",
  "valentina16flomen@gmail.com",
];

module.exports.sendEmail = async (email, membership, token) => {
  if (!emails.includes(email)) return;
  let domain = properties.isProduction()
    ? "http://bikini-fitness-front-prod.s3-website.us-east-1.amazonaws.com/"
    : "http://bikini-fitness-front.s3-website.us-east-2.amazonaws.com/";

  let basePath = domain + "newpassword/";
  let url = basePath + "?token=" + token;
  const msg = {
    to: email,
    from: "digital@bikinifitnessclub.com",
    subject: "Bikini Fitness Club - Membership Confirmation",
    html:
      "<strong>Thank you for your purchase</strong>" +
      "<p>Below are your login details to book your classes: </p>" +
      "<p>Username: " +
      email +
      " </p>" +
      "<p>Membership: " +
      membership +
      "</p>" +
      "<br>" +
      "<p>Login link: " +
      url +
      "</p>",
  };

  await sgMail.send(msg);
};

module.exports.sendEmailTrainer = async (email, token, userName) => {
  if (!emails.includes(email)) return;
  let domain = properties.isProduction()
    ? "http://bikini-fitness-front-prod.s3-website.us-east-1.amazonaws.com/"
    : "https://portal-test.appbikinifitnessclub.com/";

  let basePath = domain + "newpassword/";
  let url = basePath + "?token=" + token;
  const msg = {
    to: email,
    from: "digital@bikinifitnessclub.com",
    subject: "Bikini Fitness Club - Welcome " + userName,
    html:
      "<strong>Thank you for your registration</strong>" +
      "<p>Below is your email and a link for you to assign your password: </p>" +
      "<p>Email/Username: " +
      email +
      " </p>" +
      "<br>" +
      "<p>Login link: " +
      url +
      "</p>",
  };

  await sgMail.send(msg);
};

module.exports.sendEmailClient = async (email, token, userName) => {
  if (!emails.includes(email)) return;
  let domain = properties.isProduction()
    ? "http://bikini-fitness-front-prod.s3-website.us-east-1.amazonaws.com/"
    : "https://portal-test.appbikinifitnessclub.com/";

  let basePath = domain + "newpassword/";
  let url = basePath + "?token=" + token;
  const msg = {
    to: email,
    from: "digital@bikinifitnessclub.com",
    subject: "Bikini Fitness Club - Welcome " + userName,
    html:
      "<strong>Thank you for your registration</strong>" +
      "<p>Below is your email and a link for you to assign your password: </p>" +
      "<p>Email/Username: " +
      email +
      " </p>" +
      "<br>" +
      "<p>Login link: " +
      url +
      "</p>",
  };

  await sgMail.send(msg);
};

module.exports.sendPaymentEmail = async (email) => {
  if (!emails.includes(email)) return;
  let url = "https://buy.stripe.com/7sI28Y4gnahO58IbKB";
  const msg = {
    to: email,
    from: "digital@bikinifitnessclub.com",
    subject: "Bikini Fitness Club - Welcome",
    html:
      "<strong>Thank you for registering on our platform</strong>" +
      "<p>Below is the link for you to make the membership payment: </p>" +
      "<p>link: " +
      url +
      " </p>",
  };

  await sgMail.send(msg);
};

/**
 * Method to create the email that is sent to the user to change the password
 *
 * @param email
 * @param token
 * @returns {Promise<void>}
 */
module.exports.resetPassword = async (email, token) => {
  if (!emails.includes(email)) return;
  let domain = properties.isProduction()
    ? "http://bikini-fitness-front-prod.s3-website.us-east-1.amazonaws.com/"
    : "https://portal-test.appbikinifitnessclub.com/";

  let basePath = domain + "newpassword/";
  let url = basePath + "?token=" + token;
  const msg = {
    to: email,
    from: "digital@bikinifitnessclub.com",
    subject: "Bikini Fitness Club - Reset Password",
    html:
      "<strong>Welcome to our platform</strong>" +
      "<p>Below we leave you the link for you to change your password: </p>" +
      "<p>link: " +
      url +
      " </p>",
  };

  await sgMail.send(msg);
};

/**
 * Method to create the email that is sent to the user to cancel class
 *
 * @param email
 * @param userName
 * @param className
 * @param date
 * @returns {Promise<void>}
 */
module.exports.cancelClass = async (email, userName, className, date) => {
  if (!emails.includes(email)) return;
  const msg = {
    to: email,
    from: "digital@bikinifitnessclub.com",
    subject: "Bikini Fitness Club - Class Canceled",
    html:
      "<strong>Notification Email</strong>" +
      "<p>We inform that " +
      userName +
      " canceled her class " +
      className +
      ".</p>" +
      "<p>Date: " +
      date +
      " </p>",
  };

  await sgMail.send(msg);
};

/**
 * Method to create the email that is sent to the user to cancel class
 *
 * @param email
 * @param userName
 * @param className
 * @param date
 * @returns {Promise<void>}
 */
module.exports.reserveClass = async (email, userName, className, date) => {
  if (!emails.includes(email)) return;
  const msg = {
    to: email,
    from: "digital@bikinifitnessclub.com",
    subject: "Bikini Fitness Club - Class reserved",
    html:
      "<strong>Notification Email</strong>" +
      "<p>We inform that " +
      userName +
      " reserved her class " +
      className +
      ".</p>" +
      "<p>Date: " +
      date +
      " </p>",
  };

  await sgMail.send(msg);
};
