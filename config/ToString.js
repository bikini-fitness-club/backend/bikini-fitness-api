function toString(o) {
    Object.keys(o).forEach(k => {
        if (typeof o[k] === 'object') {
            return toString(o[k]);
        }
        o[k] = '' + o[k];
    });
    return o;
}

module.exports = toString;