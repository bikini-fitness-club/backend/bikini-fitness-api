module.exports.undefined = (data) => {
    return data === undefined;
}

module.exports.undefinedAndEmpty = (data) => {
    return data === undefined || data.trim().length === 0;
}

module.exports.isNotEmail = (data) => {
    return !data.includes("@");
}

module.exports.valueOrUndefined = (data) => {
    return data === undefined || data === null || data === '' ? undefined : data;
}