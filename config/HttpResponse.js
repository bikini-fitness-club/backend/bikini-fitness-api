module.exports.success = (res, data) => {
    return res.status(200).json({
        statusCode: 200, data, message: "Success"
    });
}

module.exports.created = (res, data) => {
    return res.status(201).json({
        statusCode: 200, data, message: "Created"
    });
}

module.exports.notFound = (res, data) => {
    return res.status(404).json({
        statusCode: 400, data, message: "Not Found"
    });
}

module.exports.badRequest = (res, data) => {
    return res.status(400).json({
        statusCode: 400, data, message: "Bad Request"
    });
}
