"use strict";

const generator = require('generate-password');

module.exports.generate = async () => {
    return generator.generate({
        length: 10,
        numbers: true,
        symbols: true
    });
}