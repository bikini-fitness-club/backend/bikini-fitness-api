"use strict";

const moment = require("moment");
const {PutItemCommand, ExecuteStatementCommand, GetItemCommand} = require("@aws-sdk/client-dynamodb");
const uuid = require("uuid");
const unwrapStructure = require("../config/UnwrapStructure");
const client = require("../config/DynamoDBConfig").getClient();

module.exports.create = async (entity) => {
    let date = moment().format();
    let id = uuid.v4();
    const command = new PutItemCommand({
        "TableName": "classesScheduler",
        "Item": {
            "id": {"S": id},
            "campusId": {"S": entity.campusId},
            "classId": {"S": entity.classId},
            "classMode": {"S": entity.classMode},
            "userId": {"S": entity.userId},
            "userLimit": {"N": entity.userLimit},
            "initialDate": {"S": entity.initialDate},
            "initialHour": {"S": entity.initialHour},
            "duration": {"N": entity.duration},
            "description": {"S": entity.description},
            "recurrent": {"N": entity.recurrent},
            "numberWeeks": {"N": entity.numberWeeks},
            "monday": {"N": entity.monday},
            "tuesday": {"N": entity.tuesday},
            "wednesday": {"N": entity.wednesday},
            "thursday": {"N": entity.thursday},
            "friday": {"N": entity.friday},
            "saturday": {"N": entity.saturday},
            "sunday": {"N": entity.sunday},
            "createdDate": {"S": date},
            "updateDate": {"S": date},
            "status": {"S": "A"}
        }
    });

    await client.send(command);
    return id;
}

module.exports.list = async () => {
    const command = new ExecuteStatementCommand({
        "Statement": "SELECT * FROM classesScheduler WHERE status = 'A'"
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.get = async (id) => {
    const command = new GetItemCommand({
        "TableName": "classesScheduler",
        "Key": {
            "id": {
                "S": id
            }
        }
    });
    return client.send(command);
}

module.exports.update = async (entity) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": "classesScheduler",
        "Item": {
            "id": {"S": entity.id},
            "campusId": {"S": entity.campusId},
            "classId": {"S": entity.classId},
            "classMode": {"S": entity.classMode},
            "userId": {"S": entity.userId},
            "userLimit": {"N": entity.userLimit},
            "initialDate": {"S": entity.initialDate},
            "initialHour": {"S": entity.initialHour},
            "duration": {"N": entity.duration},
            "description": {"S": entity.description},
            "recurrent": {"N": entity.recurrent},
            "numberWeeks": {"N": entity.numberWeeks},
            "monday": {"N": entity.monday},
            "tuesday": {"N": entity.tuesday},
            "wednesday": {"N": entity.wednesday},
            "thursday": {"N": entity.thursday},
            "friday": {"N": entity.friday},
            "saturday": {"N": entity.saturday},
            "sunday": {"N": entity.sunday},
            "createdDate": {"S": entity.createdDate},
            "updateDate": {"S": date},
            "status": {"S": entity.status}
        }
    });

    await client.send(command);
}
