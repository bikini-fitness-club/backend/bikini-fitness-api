"use strict";

const moment = require("moment");
const {
    PutItemCommand,
    ExecuteStatementCommand,
    GetItemCommand,
    DeleteItemCommand
} = require("@aws-sdk/client-dynamodb");
const uuid = require("uuid");
const unwrapStructure = require("../config/UnwrapStructure");
const client = require("../config/DynamoDBConfig").getClient();
const formatDate = "YYYY-MM-DD";

module.exports.createDetail = async (classSchedulerId, entity) => {
    try {
        let date = moment().format();
        const command = new PutItemCommand({
            "TableName": "classesSchedulerDetails",
            "Item": {
                "id": {"S": uuid.v4()},
                "classSchedulerId": {"S": classSchedulerId},
                "campusId": {"S": entity.campusId},
                "campusName": {"S": entity.campusName},
                "classId": {"S": entity.classId},
                "className": {"S": entity.className},
                "classMode": {"S": entity.classMode},
                "userId": {"S": entity.userId},
                "userName": {"S": entity.userName},
                "userLimit": {"N": entity.userLimit},
                "participants": {"N": entity.participants},
                "initialDate": {"S": entity.initialDate},
                "initialHour": {"S": entity.initialHour},
                "duration": {"N": entity.duration},
                "description": {"S": entity.description},
                "createdDate": {"S": date},
                "updateDate": {"S": date}
            }
        });

        await client.send(command);
    } catch (e) {
        console.error(e);
    }
}

module.exports.updateDetail = async (entity) => {
    try {
        let date = moment().format();
        const command = new PutItemCommand({
            "TableName": "classesSchedulerDetails",
            "Item": {
                "id": {"S": entity.id},
                "classSchedulerId": {"S": entity.classSchedulerId},
                "campusId": {"S": entity.campusId},
                "campusName": {"S": entity.campusName},
                "classId": {"S": entity.classId},
                "className": {"S": entity.className},
                "classMode": {"S": entity.classMode},
                "userId": {"S": entity.userId},
                "userName": {"S": entity.userName},
                "userLimit": {"N": entity.userLimit},
                "participants": {"N": entity.participants},
                "initialDate": {"S": entity.initialDate},
                "initialHour": {"S": entity.initialHour},
                "duration": {"N": entity.duration},
                "description": {"S": entity.description},
                "createdDate": {"S": entity.createdDate},
                "updateDate": {"S": date}
            }
        });

        await client.send(command);
    } catch (e) {
        console.error(e);
    }
}

/**
 * List of classes created from the calendar
 *
 * @param classId
 * @param trainerId
 * @param locationId
 * @param classType
 * @param date
 * @returns {Promise<*[]>}
 */
module.exports.listClassDetail = async (classId, trainerId, locationId, classType, date) => {
    let today = moment().format("YYYY-MM-DD");
    let query = "SELECT * FROM classesSchedulerDetails ";

    if (date !== undefined) {
        query += "WHERE initialDate = '" + date + "' ";
    } else {
        query += "WHERE initialDate >= '" + today + "' ";
    }

    if (classId !== undefined) {
        query += "AND classId = '" + classId + "' ";
    }

    if (trainerId !== undefined) {
        query += "AND userId = '" + trainerId + "' ";
    }

    if (locationId !== undefined) {
        query += "AND campusId = '" + locationId + "' ";
    }

    if (classType !== undefined) {
        query += "AND classMode = '" + classType.toUpperCase() + "' ";
    }

    const command = new ExecuteStatementCommand({
        "Statement": query
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.listClassPerDateAndRangeHour = async (date, initialHour, finalHour, campusId) => {
    let query = "SELECT * FROM classesSchedulerDetails WHERE initialDate = '" + date + "' ";
    let filters = "and initialHour >= '" + initialHour + "' and initialHour < '" + finalHour + "' ";
    let campusFilter = "and campusId = '" + campusId + "'";

    const command = new ExecuteStatementCommand({
        "Statement": (query + filters + campusFilter)
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.getItem = async (id) => {
    try {
        const command = new GetItemCommand({
            "TableName": "classesSchedulerDetails",
            "Key": {
                "id": {
                    "S": id
                }
            }
        });
        let result = await client.send(command);
        return result.Item !== undefined ? unwrapStructure(result.Item) : undefined;
    } catch (e) {
        console.error(e);
    }
}

module.exports.delete = async (classSchedulerId) => {
    try {
        const command = new DeleteItemCommand({
            "TableName": "classesSchedulerDetails",
            "Key": {
                "id": {
                    "S": classSchedulerId
                }
            }
        });

        await client.send(command);
    } catch (e) {
        console.error(e);
    }
}

/**
 *
 *
 * @returns {Promise<*[]>}
 */
module.exports.listPersonalClass = async (campusId, initialDate, finalDate) => {
    let i = moment(initialDate, "DD/MM/YYYY").format(formatDate);
    let f = moment(finalDate, "DD/MM/YYYY").format(formatDate);

    let query = "SELECT * FROM classesSchedulerDetails WHERE classMode = '" + "P" + "' ";
    query += "AND initialDate >= '" + i + "' ";
    query += "AND initialDate <= '" + f + "' ";
    query += "AND campusId = '" + campusId + "'";

    const command = new ExecuteStatementCommand({
        "Statement": query
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

/**
 *
 *
 * @returns {Promise<*[]>}
 */
module.exports.listGroupClass = async (campusId, initialDate, finalDate) => {
    let i = moment(initialDate, "DD/MM/YYYY").format(formatDate);
    let f = moment(finalDate, "DD/MM/YYYY").format(formatDate);

    let query = "SELECT * FROM classesSchedulerDetails WHERE classMode = '" + "G" + "' ";
    query += "AND initialDate >= '" + i + "' ";
    query += "AND initialDate <= '" + f + "' ";
    query += "AND campusId = '" + campusId + "'";

    const command = new ExecuteStatementCommand({
        "Statement": query
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

/**
 *
 * @param locationId
 * @returns {Promise<*[]>}
 */
module.exports.listClassPerLocationId = async (locationId) => {
    let today = moment().format("YYYY-MM-DD");
    let query = "SELECT * FROM classesSchedulerDetails ";
    query += "WHERE initialDate >= '" + today + "' ";
    query += "AND campusId = '" + locationId + "' ";

    const command = new ExecuteStatementCommand({
        "Statement": query
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

/**
 *
 * @param userId
 * @param campusId
 */
module.exports.listClassPerUserIdAndCampusId = async (userId, campusId) => {
    let query = "SELECT * FROM classesSchedulerDetails ";
    query += "WHERE userId = '" + userId + "' ";
    query += "AND campusId = '" + campusId + "' ";

    const command = new ExecuteStatementCommand({
        "Statement": query
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}