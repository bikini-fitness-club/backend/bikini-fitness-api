"use strict";


const moment = require("moment/moment");
const {PutItemCommand, ExecuteStatementCommand} = require("@aws-sdk/client-dynamodb");
const uuid = require("uuid");
const unwrapStructure = require("../config/UnwrapStructure");
const client = require("../config/DynamoDBConfig").getClient();


module.exports.create = async (entity) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": "token",
        "Item": {
            "id": {"S": uuid.v4()},
            "userId": {"S": entity.userId},
            "token": {"S": entity.token},
            "createdDate": {"S": date},
            "updateDate": {"S": date},
            "status": {"S": "A"}
        }
    });

    await client.send(command);
}

module.exports.update = async (entity) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": "token",
        "Item": {
            "id": {"S": entity.id},
            "userId": {"S": entity.userId},
            "token": {"S": entity.token},
            "createdDate": {"S": entity.createdDate},
            "updateDate": {"S": date},
            "status": {"S": entity.status}
        }
    });

    await client.send(command);
}

module.exports.get = async (token) => {
    let query = "SELECT * FROM token WHERE token = '" + token + "' and status = 'A'";
    const command = new ExecuteStatementCommand({
        "Statement": query
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}