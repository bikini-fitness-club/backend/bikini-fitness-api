"use strict";

const moment = require("moment");
const {PutItemCommand, ExecuteStatementCommand, GetItemCommand} = require("@aws-sdk/client-dynamodb");
const uuid = require("uuid");
const unwrapStructure = require("../config/UnwrapStructure");
const client = require("../config/DynamoDBConfig").getClient();


module.exports.create = async (entity) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": "memberships",
        "Item": {
            "id": {"S": uuid.v4()},
            "name": {"S": entity.name},
            "relationalId": {"N": entity.relationalId},
            "duration": {"S": entity.duration},
            "unitTime": {"S": entity.unitTime},
            "hasGroupClass": {"N": entity.hasGroupClass},
            "unlimitedGroupClasses": {"N": entity.unlimitedGroupClasses},
            "numberGroupClasses": {"N": entity.numberGroupClasses},
            "unitTimeGroupClasses": {"S": entity.unitTimeGroupClasses},
            "hasPersonalClass": {"N": entity.hasPersonalClass},
            "unlimitedPersonalClasses": {"N": entity.unlimitedPersonalClasses},
            "numberPersonalClasses": {"N": entity.numberPersonalClasses},
            "unitTimePersonalClasses": {"S": entity.unitTimePersonalClasses},
            "url": {"S": ""},
            "cost": {"S": entity.cost},
            "createdDate": {"S": date},
            "updateDate": {"S": date},
            "status": {"S": "A"}
        }
    });

    await client.send(command);
}

module.exports.list = async () => {
    const command = new ExecuteStatementCommand({
        "Statement": "SELECT * FROM memberships WHERE status = 'A'"
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.listAll = async () => {
    const command = new ExecuteStatementCommand({
        "Statement": "SELECT * FROM memberships"
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.get = async (id) => {
    const command = new GetItemCommand({
        "TableName": "memberships",
        "Key": {
            "id": {
                "S": id
            }
        }
    });
    return client.send(command);
}

module.exports.getItem = async (id) => {
    const command = new GetItemCommand({
        "TableName": "memberships",
        "Key": {
            "id": {
                "S": id
            }
        }
    });
    let result = await client.send(command);
    return result.Item !== undefined ? unwrapStructure(result.Item) : undefined;
}

module.exports.update = async (entity) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": "memberships",
        "Item": {
            "id": {"S": entity.id},
            "name": {"S": entity.name},
            "relationalId": {"N": entity.relationalId},
            "duration": {"S": entity.duration},
            "unitTime": {"S": entity.unitTime},
            "hasGroupClass": {"N": entity.hasGroupClass},
            "unlimitedGroupClasses": {"N": entity.unlimitedGroupClasses},
            "numberGroupClasses": {"N": entity.numberGroupClasses},
            "unitTimeGroupClasses": {"S": entity.unitTimeGroupClasses},
            "hasPersonalClass": {"N": entity.hasPersonalClass},
            "unlimitedPersonalClasses": {"N": entity.unlimitedPersonalClasses},
            "numberPersonalClasses": {"N": entity.numberPersonalClasses},
            "unitTimePersonalClasses": {"S": entity.unitTimePersonalClasses},
            "url": {"S": entity.url},
            "cost": {"S": entity.cost},
            "createdDate": {"S": entity.createdDate},
            "updateDate": {"S": date},
            "status": {"S": entity.status}
        }
    });

    await client.send(command);
}

module.exports.findByRelationId = async (relationId) => {
    const command = new ExecuteStatementCommand({
        "Statement": "SELECT * FROM memberships WHERE relationalId = " + relationId
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

