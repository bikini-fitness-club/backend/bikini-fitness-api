"use strict";

const moment = require("moment/moment");
const {PutItemCommand, ExecuteStatementCommand, GetItemCommand} = require("@aws-sdk/client-dynamodb");
const uuid = require("uuid");
const unwrapStructure = require("../config/UnwrapStructure");
const client = require("../config/DynamoDBConfig").getClient();

module.exports.createClassModel = async (classModel) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": "classModels",
        "Item": {
            "id": {"S": uuid.v4()},
            "name": {"S": classModel.name},
            "createdDate": {"S": date},
            "updateDate": {"S": date},
            "status": {"S": "A"}
        }
    });

    await client.send(command);
}

module.exports.createClassType = async (classType) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": "classTypes",
        "Item": {
            "id": {"S": uuid.v4()},
            "name": {"S": classType.name},
            "createdDate": {"S": date},
            "updateDate": {"S": date},
            "status": {"S": "A"}
        }
    });

    await client.send(command);
}

module.exports.createClass = async (clazz) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": "classes",
        "Item": {
            "id": {"S": uuid.v4()},
            "typeId": {"S": clazz.typeId},
            "modelId": {"S": clazz.modelId},
            "name": {"S": clazz.name},
            "showPersonalClassList": {"N": clazz.showPersonalClassList},
            "description": {"S": clazz.description},
            "createdDate": {"S": date},
            "updateDate": {"S": date},
            "status": {"S": "A"}
        }
    });

    await client.send(command);
}

module.exports.listClassModel = async () => {
    const command = new ExecuteStatementCommand({
        "Statement": "SELECT * FROM classModels WHERE status = 'A'"
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.listClassType = async () => {
    const command = new ExecuteStatementCommand({
        "Statement": "SELECT * FROM classTypes WHERE status = 'A'"
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.listClass = async () => {
    const command = new ExecuteStatementCommand({
        "Statement": "SELECT * FROM classes WHERE status = 'A'"
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.listAll = async () => {
    const command = new ExecuteStatementCommand({
        "Statement": "SELECT * FROM classes"
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.get = async (id, entity) => {
    const command = new GetItemCommand({
        "TableName": entity,
        "Key": {
            "id": {
                "S": id
            }
        }
    });
    return client.send(command);
}

module.exports.getItem = async (id) => {
    const command = new GetItemCommand({
        "TableName": "classes",
        "Key": {
            "id": {
                "S": id
            }
        }
    });
    let result = await client.send(command);
    return result.Item !== undefined ? unwrapStructure(result.Item) : undefined;
}

module.exports.updateClassModel = async (classModel) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": "classModels",
        "Item": {
            "id": {"S": classModel.id},
            "name": {"S": classModel.name},
            "createdDate": {"S": classModel.createdDate},
            "updateDate": {"S": date},
            "status": {"S": classModel.status}
        }
    });

    await client.send(command);
}

module.exports.updateClassType = async (classType) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": "classTypes",
        "Item": {
            "id": {"S": classType.id},
            "name": {"S": classType.name},
            "createdDate": {"S": classType.createdDate},
            "updateDate": {"S": date},
            "status": {"S": classType.status}
        }
    });

    await client.send(command);
}

module.exports.updateClass = async (clazz) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": "classes",
        "Item": {
            "id": {"S": clazz.id},
            "typeId": {"S": clazz.typeId},
            "modelId": {"S": clazz.modelId},
            "name": {"S": clazz.name},
            "showPersonalClassList": {"N": clazz.showPersonalClassList},
            "description": {"S": clazz.description},
            "createdDate": {"S": clazz.createdDate},
            "updateDate": {"S": date},
            "status": {"S": clazz.status}
        }
    });

    await client.send(command);
}