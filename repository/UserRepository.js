"use strict";

const {PutItemCommand, ExecuteStatementCommand, GetItemCommand} = require("@aws-sdk/client-dynamodb");
const uuid = require('uuid');
const tableName = "users";
const unwrapStructure = require("../config/UnwrapStructure");
const moment = require('moment');
const client = require("../config/DynamoDBConfig").getClient();

module.exports.register = async (user) => {
    let date = moment().format();
    let userId = uuid.v4();
    const command = new PutItemCommand({
        "TableName": tableName,
        "Item": {
            "id": {"S": userId},
            "firstName": {"S": user.first_name},
            "lastName": {"S": user.last_name},
            "email": {"S": user.email},
            "address": {"S": user.default_address.address1},
            "phone": {"S": user.default_address.phone},
            "mobile": {"S": ""},
            "password": {"S": user.password},
            "profileId": {"S": "51f2a841-a3ff-4345-a3b6-ab082aea2c56"},
            "source": {"S": "SHOPIFY"},
            "createdDate": {"S": date},
            "updateDate": {"S": date},
            "status": {"S": "A"}
        }
    });

    await client.send(command);
    return userId;
}

module.exports.findByEmail = async (email) => {
    let query = "SELECT * FROM users WHERE email='" + email + "' and status = 'A'";
    const command = new ExecuteStatementCommand({
        "Statement": query
    });

    let output = await client.send(command);
    let users = [];
    if (output.Items.length > 0) {
        output.Items.forEach(item => {
            users.push(unwrapStructure(item));
        });
    }

    return users;
}

module.exports.findAllByEmail = async (email) => {
    let query = "SELECT * FROM users WHERE email='" + email + "'";
    const command = new ExecuteStatementCommand({
        "Statement": query
    });

    let output = await client.send(command);
    let users = [];
    if (output.Items.length > 0) {
        output.Items.forEach(item => {
            users.push(unwrapStructure(item));
        });
    }

    return users;
}

module.exports.create = async (entity) => {
    let date = moment().format();
    let userId = uuid.v4();
    const command = new PutItemCommand({
        "TableName": tableName,
        "Item": {
            "id": {"S": userId},
            "firstName": {"S": entity.firstName},
            "lastName": {"S": entity.lastName},
            "email": {"S": entity.email},
            "address": {"S": entity.address},
            "phone": {"S": entity.phone},
            "mobile": {"S": ""},
            "password": {"S": entity.password},
            "profileId": {"S": entity.profileId},
            "source": {"S": "BACKOFFICE"},
            "createdDate": {"S": date},
            "updateDate": {"S": date},
            "status": {"S": entity.status}
        }
    });

    await client.send(command);
    return userId;
}

module.exports.get = async (id) => {
    const command = new GetItemCommand({
        "TableName": tableName,
        "Key": {
            "id": {
                "S": id
            }
        }
    });
    return client.send(command);
}

module.exports.getItem = async (id) => {
    const command = new GetItemCommand({
        "TableName": tableName,
        "Key": {
            "id": {
                "S": id
            }
        }
    });
    let result = await client.send(command);
    return result.Item !== undefined ? unwrapStructure(result.Item) : undefined;
}

module.exports.update = async (entity) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": tableName,
        "Item": {
            "id": {"S": entity.id},
            "firstName": {"S": entity.firstName},
            "lastName": {"S": entity.lastName},
            "email": {"S": entity.email},
            "address": {"S": entity.address},
            "phone": {"S": entity.phone},
            "password": {"S": entity.password},
            "profileId": {"S": entity.profileId},
            "createdDate": {"S": entity.createdDate},
            "updateDate": {"S": date},
            "status": {"S": entity.status}
        }
    });

    await client.send(command);
}

module.exports.list = async (profileId) => {
    let query = "SELECT * FROM users WHERE status != 'I' and profileId = '" + profileId + "'";
    const command = new ExecuteStatementCommand({
        "Statement": query
    });

    let output = await client.send(command);
    let users = [];
    output.Items.forEach(user => {
        users.push(unwrapStructure(user));
    });

    return users;
}

module.exports.migrate = async (entity) => {
    let date = moment().format();
    let userId = uuid.v4();
    const command = new PutItemCommand({
        "TableName": tableName,
        "Item": {
            "id": {"S": userId},
            "firstName": {"S": entity.firstName},
            "lastName": {"S": entity.lastName},
            "email": {"S": entity.email},
            "address": {"S": entity.address},
            "phone": {"S": entity.phone},
            "mobile": {"S": ""},
            "password": {"S": entity.password},
            "profileId": {"S": entity.profileId},
            "source": {"S": "MIGRATED"},
            "createdDate": {"S": date},
            "updateDate": {"S": date},
            "status": {"S": entity.status}
        }
    });

    await client.send(command);
    return userId;
}