const moment = require("moment/moment");
const {PutItemCommand, ExecuteStatementCommand} = require("@aws-sdk/client-dynamodb");
const uuid = require("uuid");
const unwrapStructure = require("../config/UnwrapStructure");
const client = require("../config/DynamoDBConfig").getClient();

module.exports.create = async (entity) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": "userCampus",
        "Item": {
            "id": {"S": uuid.v4()},
            "userId": {"S": entity.userId},
            "campusId": {"S": entity.campusId},
            "createdDate": {"S": date},
            "updateDate": {"S": date},
        }
    });

    await client.send(command);
}

module.exports.findByUserId = async (userId) => {
    let query = "SELECT * FROM userCampus WHERE userId='" + userId + "'";
    const command = new ExecuteStatementCommand({
        "Statement": query
    });

    let output = await client.send(command);
    let users = [];
    if (output.Items.length > 0) {
        output.Items.forEach(item => {
            users.push(unwrapStructure(item));
        });
    }

    return users;
}