"use strict";

const {PutItemCommand, ExecuteStatementCommand, GetItemCommand} = require("@aws-sdk/client-dynamodb");
const unwrapStructure = require("../config/UnwrapStructure");
const uuid = require('uuid');
const moment = require("moment");
const tableName = "campus";
const client = require("../config/DynamoDBConfig").getClient();
const ShortUniqueId = require('short-unique-id');

const { randomUUID } = new ShortUniqueId({ length: 8 });

module.exports.listIds = async () => {
    const command = new ExecuteStatementCommand({
        "Statement": "SELECT id FROM campus WHERE status = 'A'"
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.list = async () => {
    const command = new ExecuteStatementCommand({
        "Statement": "SELECT * FROM campus WHERE status = 'A'"
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.listAll = async () => {
    const command = new ExecuteStatementCommand({
        "Statement": "SELECT * FROM campus"
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.create = async (campus) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": tableName,
        "Item": {
            "id": {"S": uuid.v4()},
            "sku": {"S": `MSHIP-${randomUUID().toUpperCase()}`},
            "name": {"S": campus.name.toLowerCase()},
            "address": {"S": campus.address},
            "postalCode": {"S": campus.postalCode},
            "phone": {"S": campus.phone},
            "mobile": {"S": campus.mobile},
            "email": {"S": campus.email},
            "createdDate": {"S": date},
            "updateDate": {"S": date},
            "status": {"S": "A"}
        }
    });

    await client.send(command);
}

module.exports.get = async (campusId) => {
    const command = new GetItemCommand({
        "TableName": tableName,
        "Key": {
            "id": {
                "S": campusId
            }
        }
    });
    return client.send(command);
}

module.exports.getItem = async (id) => {
    const command = new GetItemCommand({
        "TableName": tableName,
        "Key": {
            "id": {
                "S": id
            }
        }
    });
    let result = await client.send(command);
    return result.Item !== undefined ? unwrapStructure(result.Item) : undefined;
}

module.exports.update = async (campus) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": tableName,
        "Item": {
            "id": {"S": campus.id},
            "name": {"S": campus.name},
            "address": {"S": campus.address},
            "postalCode": {"S": campus.postalCode},
            "phone": {"S": campus.phone},
            "mobile": {"S": campus.mobile},
            "email": {"S": campus.email},
            "createdDate": {"S": campus.createdDate},
            "updateDate": {"S": date},
            "status": {"S": campus.status}
        }
    });

    try {
        await client.send(command);
    } catch (exception) {
        console.error(exception);
    }

}

module.exports.getByName = async (campusName) => {
    let query = "SELECT * FROM campus WHERE name='" + campusName + "'";
    const command = new ExecuteStatementCommand({
        "Statement": query
    });

    let output = await client.send(command);
    let campus = [];
    if (output.Items.length > 0) {
        output.Items.forEach(item => {
            campus.push(unwrapStructure(item));
        });
    }

    return campus;
}

module.exports.getBySKU = async (sku) => {
    let query = "SELECT * FROM campus WHERE sku='" + sku + "'";
    const command = new ExecuteStatementCommand({
        "Statement": query
    });

    let output = await client.send(command);
    let campus = [];
    if (output.Items.length > 0) {
        output.Items.forEach(item => {
            campus.push(unwrapStructure(item));
        });
    }

    return campus;
}