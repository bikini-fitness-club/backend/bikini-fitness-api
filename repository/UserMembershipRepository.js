"use strict";

const moment = require("moment");
const {DynamoDBClient, PutItemCommand, ExecuteStatementCommand, GetItemCommand} = require("@aws-sdk/client-dynamodb");
const uuid = require("uuid");
const unwrapStructure = require("../config/UnwrapStructure");
const client = require("../config/DynamoDBConfig").getClient();


module.exports.associate = async (entity) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": "userMembership",
        "Item": {
            "id": {"S": uuid.v4()},
            "userId": {"S": entity.userId},
            "membershipId": {"S": entity.membershipId},
            "initialDate": {"S": entity.initialDate},
            "finalDate": {"S": entity.finalDate},
            "paymentId": {"S": ""},
            "createdDate": {"S": date},
            "updateDate": {"S": date},
            "status": {"S": "A"}
        }
    });

    await client.send(command);
}

module.exports.findByUserId = async (userId) => {
    let query = "SELECT * FROM userMembership WHERE userId='" + userId + "' and status = 'A'";
    const command = new ExecuteStatementCommand({
        "Statement": query
    });

    let output = await client.send(command);
    let items = [];
    if (output.Items.length > 0) {
        output.Items.forEach(item => {
            items.push(unwrapStructure(item));
        });
    }

    return items;
}

module.exports.getItem = async (id) => {
    const command = new GetItemCommand({
        "TableName": "userMembership",
        "Key": {
            "id": {
                "S": id
            }
        }
    });
    let result = await client.send(command);
    return result.Item !== undefined ? unwrapStructure(result.Item) : undefined;
}

module.exports.update = async (entity) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": "userMembership",
        "Item": {
            "id": {"S": entity.id},
            "userId": {"S": entity.userId},
            "membershipId": {"S": entity.membershipId},
            "initialDate": {"S": entity.initialDate},
            "finalDate": {"S": entity.finalDate},
            "paymentId": {"S": entity.paymentId},
            "createdDate": {"S": entity.createdDate},
            "updateDate": {"S": date},
            "status": {"S": entity.status}
        }
    });

    await client.send(command);
}

module.exports.findAllByUserId = async (userId) => {
    let query = "SELECT * FROM userMembership WHERE userId = '" + userId + "'";
    const command = new ExecuteStatementCommand({
        "Statement": query
    });

    let output = await client.send(command);
    let items = [];
    if (output.Items.length > 0) {
        output.Items.forEach(item => {
            items.push(unwrapStructure(item));
        });
    }

    return items;
}
