"use strict";

const moment = require("moment");
const {PutItemCommand, ExecuteStatementCommand, GetItemCommand} = require("@aws-sdk/client-dynamodb");
const uuid = require("uuid");
const unwrapStructure = require("../config/UnwrapStructure");
const client = require("../config/DynamoDBConfig").getClient();


module.exports.create = async (entity) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": "profiles",
        "Item": {
            "id": {"S": uuid.v4()},
            "name": {"S": entity.name},
            "createdDate": {"S": date},
            "updateDate": {"S": date},
            "status": {"S": "A"}
        }
    });

    await client.send(command);
}

module.exports.list = async () => {
    const command = new ExecuteStatementCommand({
        "Statement": "SELECT * FROM profiles WHERE status = 'A'"
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.get = async (id) => {
    const command = new GetItemCommand({
        "TableName": "profiles",
        "Key": {
            "id": {
                "S": id
            }
        }
    });
    return client.send(command);
}

module.exports.getItem = async (id) => {
    const command = new GetItemCommand({
        "TableName": "profiles",
        "Key": {
            "id": {
                "S": id
            }
        }
    });
    let result = await client.send(command);
    return result.Item !== undefined ? unwrapStructure(result.Item) : undefined;
}

module.exports.update = async (entity) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": "profiles",
        "Item": {
            "id": {"S": entity.id},
            "name": {"S": entity.name},
            "createdDate": {"S": entity.createdDate},
            "updateDate": {"S": date},
            "status": {"S": entity.status}
        }
    });

    await client.send(command);
}

