const moment = require("moment/moment");
const {DynamoDBClient, PutItemCommand} = require("@aws-sdk/client-dynamodb");
const uuid = require("uuid");
const client = require("../config/DynamoDBConfig").getClient();

module.exports.create = async (entity) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": "classesFree",
        "Item": {
            "id": {"S": uuid.v4()},
            "userId": {"S": entity.userId},
            "email": {"S": entity.email},
            "classScheduleId": {"S": entity.classScheduleId},
            "createdDate": {"S": date}
        }
    });

    await client.send(command);
}