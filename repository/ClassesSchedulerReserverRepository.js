"use strict";

const moment = require("moment");
const {
    PutItemCommand,
    ExecuteStatementCommand,
    GetItemCommand,
    DeleteItemCommand
} = require("@aws-sdk/client-dynamodb");
const uuid = require("uuid");
const unwrapStructure = require("../config/UnwrapStructure");
const client = require("../config/DynamoDBConfig").getClient();


module.exports.create = async (entity) => {
    let date = moment().format();
    const command = new PutItemCommand({
        "TableName": "classesSchedulerReserves",
        "Item": {
            "id": {"S": uuid.v4()},
            "classScheduleId": {"S": entity.classScheduleId},
            "campusId": {"S": entity.campusId},
            "classId": {"S": entity.classId},
            "classMode": {"S": entity.classMode},
            "userId": {"S": entity.userId},
            "trainerId": {"S": entity.trainerId},
            "initialDate": {"S": entity.initialDate},
            "initialHour": {"S": entity.initialHour},
            "duration": {"N": entity.duration.toString()},
            "createdDate": {"S": date},
            "updateDate": {"S": date},
            "status": {"S": "A"}
        }
    });

    await client.send(command);
}

module.exports.getPerUserId = async (userId, campusId) => {
    let query = "SELECT * FROM classesSchedulerReserves WHERE userId = '" + userId + "' AND campusId = '" + campusId + "'";
    const command = new ExecuteStatementCommand({
        "Statement": query
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.getPerTrainerId = async (userId, campusId) => {
    let query = "SELECT * FROM classesSchedulerReserves WHERE trainerId = '" + userId + "' AND campusId = '" + campusId + "'";
    const command = new ExecuteStatementCommand({
        "Statement": query
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.getPerClassScheduleId = async (classScheduleId) => {
    let query = "SELECT * FROM classesSchedulerReserves WHERE classScheduleId = '" + classScheduleId + "'";
    const command = new ExecuteStatementCommand({
        "Statement": query
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.getById = async (classScheduleReserveId) => {
    let query = "SELECT * FROM classesSchedulerReserves WHERE id = '" + classScheduleReserveId + "'";
    const command = new ExecuteStatementCommand({
        "Statement": query
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.overdueClassesPerUser = async (userId, date) => {
    let query = "SELECT * FROM classesSchedulerReserves ";
    let filters = "WHERE initialDate < '" + date + "' and userId = '" + userId + "'";
    const command = new ExecuteStatementCommand({
        "Statement": (query + filters)
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.delete = async (classScheduleReserveId) => {
    const command = new DeleteItemCommand({
        "TableName": 'classesSchedulerReserves',
        "Key": {
            "id": {
                "S": classScheduleReserveId
            }
        }
    });
    return client.send(command);
}

module.exports.getByClassScheduleIdAndUserId = async (classScheduleId, userId) => {
    let query = "SELECT * FROM classesSchedulerReserves WHERE classScheduleId = '" + classScheduleId + "' ";
    let filters = "AND userId = '" + userId + "'";
    const command = new ExecuteStatementCommand({
        "Statement": (query + filters)
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.getByDateRangeAndUserId = async (initialDate, finalDate, userId) => {
    let query = "SELECT * FROM classesSchedulerReserves ";
    let filters = "WHERE initialDate >= '" + initialDate + "' AND initialDate <= '" + finalDate + "' AND userId = '" + userId + "'";
    const command = new ExecuteStatementCommand({
        "Statement": (query + filters)
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}

module.exports.getByDateAndUser = async (date, userId) => {
    let query = "SELECT * FROM classesSchedulerReserves ";
    let filters = "WHERE initialDate = '" + date + "' AND userId = '" + userId + "'";
    const command = new ExecuteStatementCommand({
        "Statement": (query + filters)
    });

    let output = await client.send(command);
    let items = [];
    output.Items.forEach(item => {
        items.push(unwrapStructure(item));
    });

    return items;
}