const express = require("express");
const cors = require("cors");
const app = express();
const port = process.env.PORT || 8080;

const authRoute = require("./routes/AuthRoute");
const userRoute = require("./routes/UserRoute");
const indexRoute = require("./routes/IndexRoute");
const classRoute = require("./routes/ClassRoute");
const campusRoute = require("./routes/CampusRoute");
const profileRoute = require("./routes/ProfileRoute");
const dashboardRoute = require("./routes/DashboardRoute");
const migrationRoute = require("./routes/MigrationRoute");
const membershipRoute = require("./routes/MembershipRoute");
const userMembershipRoute = require("./routes/UserMembershipRoute");

app.use(
  cors({
    origin: "*",
  })
);

app.use(express.json());

// Manejador de errores global para capturar cualquier error no manejado
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).json({ error: "Something went wrong!" });
});

app.use("/api", indexRoute);
app.use("/api/auth", authRoute);
app.use("/api/users", userRoute);
app.use("/api/campus", campusRoute);
app.use("/api/classes", classRoute);
app.use("/api/dashboard", dashboardRoute);
app.use("/api/memberships", membershipRoute);
app.use("/api/users/profiles", profileRoute);
app.use("/api/users/memberships", userMembershipRoute);
app.use("/api/migration", migrationRoute);

// Manejador de errores para rutas no encontradas
app.use((req, res, next) => {
  res.status(404).json({ error: "Route not found" });
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
}).on("error", (e) => {
  console.error(`Error starting the server: ${e.message}`);
});
