const router = require("express").Router();
const controller = require("../controller/CampusController");

router.get("/", async (req, res) => {
    return await controller.list(req, res);
});

router.post("/", async (req, res) => {
    return await controller.create(req, res);
});

router.put("/", async (req, res) => {
    return await controller.update(req, res);
});

router.delete("/:id", async (req, res) => {
    return await controller.delete(req, res);
});

router.get("/users/:id", async (req, res) => {
    return await controller.getByUserId(req, res);
});

module.exports = router;