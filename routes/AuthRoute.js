const router = require("express").Router();
const authController = require("../controller/AuthController");

router.post("/login", async (req, res) => {
    return await authController.login(req, res);
});

router.post("/reset-password", async (req, res) => {
    return await authController.resetPassword(req, res);
});

router.post("/validate-email", async (req, res) => {
    return await authController.validateEmail(req, res);
});

module.exports = router;