const router = require("express").Router();
const controller = require("../controller/DashboardController");

router.get("/", async (req, res) => {
    return await controller.generate(req, res);
});

module.exports = router;