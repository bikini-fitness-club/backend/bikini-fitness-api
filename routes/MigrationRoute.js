const router = require("express").Router();
const controller = require("../controller/MigrationController");

router.post("/", async (req, res) => {
    return await controller.users(req, res);
});

module.exports = router;
