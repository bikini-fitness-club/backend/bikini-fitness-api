const router = require("express").Router();
const controller = require("../controller/ClassController");
const classesSchedulerController = require("../controller/ClassesSchedulerController");

/* Classes */
router.get("/", async (req, res) => {
    return await controller.listClass(req, res);
});

router.post("/", async (req, res) => {
    return await controller.createClass(req, res);
});

router.put("/", async (req, res) => {
    return await controller.updateClass(req, res);
});

router.delete("/:id", async (req, res) => {
    return await controller.deleteClass(req, res);
});

/* Class Models */
router.get("/models", async (req, res) => {
    return await controller.listClassModel(req, res);
});

router.post("/models", async (req, res) => {
    return await controller.createClassModel(req, res);
});

router.put("/models", async (req, res) => {
    return await controller.updateClassModel(req, res);
});

router.delete("/models/:id", async (req, res) => {
    return await controller.deleteClassModel(req, res);
});

/* Class Types */
router.get("/types", async (req, res) => {
    return await controller.listClassType(req, res);
});

router.post("/types", async (req, res) => {
    return await controller.createClassType(req, res);
});

router.put("/types", async (req, res) => {
    return await controller.updateClassType(req, res);
});

router.delete("/types/:id", async (req, res) => {
    return await controller.deleteClassType(req, res);
});

/* Class Scheduler */
router.post("/scheduler", async (req, res) => {
    return await classesSchedulerController.create(req, res);
});

router.put("/scheduler", async (req, res) => {
    return await classesSchedulerController.update(req, res);
});

router.get("/scheduler", async (req, res) => {
    return await classesSchedulerController.list(req, res);
});

router.delete("/scheduler/:id", async (req, res) => {
    return await classesSchedulerController.delete(req, res);
});

router.get("/scheduler/details/:id", async (req, res) => {
    return await classesSchedulerController.getClassesScheduler(req, res);
});

router.get("/scheduler/users", async (req, res) => {
    return await classesSchedulerController.userClassesScheduler(req, res);
});

router.post("/scheduler/users", async (req, res) => {
    return await classesSchedulerController.registerUserClassesSchedulerReserve(req, res);
});

router.get("/scheduler/users/:id", async (req, res) => {
    return await classesSchedulerController.listUserClassesSchedulerReserved(req, res);
});

router.get("/scheduler/:classId/users/:userId/validate", async (req, res) => {
    return await classesSchedulerController.validateDeleteUserClassesSchedulerReserve(req, res);
});

router.delete("/scheduler/:classId/users/:userId", async (req, res) => {
    return await classesSchedulerController.deleteUserClassesSchedulerReserve(req, res);
});

router.get("/users/scheduler/:id", async (req, res) => {
    return await classesSchedulerController.getDetailClassesScheduler(req, res);
});

router.get("/scheduler/free-class", async (req, res) => {
    return await classesSchedulerController.getClassesSchedulerFreeClass(req, res);
});

module.exports = router;