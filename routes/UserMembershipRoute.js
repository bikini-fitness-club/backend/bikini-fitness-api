const router = require("express").Router();
const controller = require("../controller/MembershipsController");

router.post("/associate", async (req, res) => {
    return await controller.associateMembership(req, res);
});

router.post("/payment-associate", async (req, res) => {
    return await controller.registerPayment(req, res);
});

module.exports = router;