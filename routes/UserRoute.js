const router = require("express").Router();
const controller = require("../controller/UserController");

router.get("/", async (req, res) => {
    return await controller.list(req, res);
});

router.post("/", async (req, res) => {
    return await controller.create(req, res);
});

router.put("/", async (req, res) => {
    return await controller.update(req, res);
});

router.delete("/:id", async (req, res) => {
    return await controller.delete(req, res);
});

router.post("/register", async (req, res) => {
    return await controller.register(req, res);
});

router.post("/free-class", async (req, res) => {
    return await controller.createUserFreeClass(req, res);
});

module.exports = router;