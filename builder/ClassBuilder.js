"use strict";

module.exports.buildClass = async (parameterClass, clazz, campus, dateClass, user) => {
    return {
        "campusId": campus.id,
        "campusName": campus.name,
        "classId": clazz.id,
        "className": clazz.name,
        "classMode": parameterClass.classMode,
        "userId": user.id,
        "userName": user.firstName + " " + user.lastName,
        "userLimit": parameterClass.userLimit,
        "participants": "0",
        "initialDate": dateClass,
        "initialHour": parameterClass.initialHour,
        "duration": parameterClass.duration,
        "description": parameterClass.description
    }
}